#!/bin/sh

set -e

for collection in $(mongosh appwhite --quiet --eval "show collections"); do
   mongoexport --db appwhite --collection $collection --jsonFormat=canonical | jq -cf /tmp/filter.jq | sed 's/\\"/\\\\"/g' > /tmp/$collection.json
   cat << EOF >> /tmp/init.sql
CREATE TABLE $collection(
 id CHAR(24) PRIMARY KEY GENERATED ALWAYS AS (data->>'_id') STORED,
 data JSONB
);
\copy $collection(data) FROM '/tmp/$collection.json'

EOF
done
