#!/bin/ash

# Ce script attend que la connexion à Postgres soit possible, ce qui indique la fin du transfert de données

TIMEOUT=600

i=0
while [ "$i" -lt "$TIMEOUT" ]; do
  let i++
  sleep 1
  nc -z localhost 5432
  [ "$?" == "0" ] && break
done

if [ "$i" == "$TIMEOUT" ]; then
  echo "Échec de la connexion à Postgres au bout de $TIMEOUT secondes" && exit 1
else
  echo "Transfert complété au bout de $i secondes"
fi

exit 0
