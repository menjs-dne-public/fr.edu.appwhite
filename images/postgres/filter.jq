# Filtre pour transformer les objets imbriqués correspondant à des types BSON
# Par exemple:
#
# "dateNaissance": {
#   "$date": {
#     "$numberLong": "-122778000000"
#   }
# }
#
# devient
#
# "dateNaissance": "-122778000000"

walk (
  if type == "object" then
    if has("$oid") then
      # Remplacer les OIDs imbriqués par des chaines de caractères
      ."$oid"
    elif has("$date") then
      # Remplacer les dates imbriquées par des nombres
      ."$date"."$numberLong" | tonumber
    elif has("$numberInt") then
      # Remplacer les entiers imbriqués par des nombres
      ."$numberInt" | tonumber
    else .
    end
  else .
  end
)
