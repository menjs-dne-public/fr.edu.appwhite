Projet d'application blanche pour MongoDB et PostgreSQL

## Prérequis

* JDK ≥ 11
* maven ≥ 3.5
* Docker
* Pour certains tests, un export de données MongoDB (détails par la suite)

```
mvn --version
Apache Maven 3.6.3
Maven home: /usr/share/maven
Java version: 14.0.2, vendor: Private Build, runtime: /usr/lib/jvm/java-14-openjdk-amd64
Default locale: fr_FR, platform encoding: UTF-8
OS name: "linux", version: "5.11.0-38-generic", arch: "amd64", family: "unix"
```

Pendant les tests, des conteneurs docker vont être lancés, il faut donc que
l'utilisateur courant puisse utiliser `docker`

```
docker --version
Docker version 20.10.7, build 20.10.7-0ubuntu1~20.04.2
```

Sous Debian ou Ubuntu, par défaut, il faut être `root` pour lancer docker
donc il convient d'autoriser l'utilisateur courant (`sylvie`) à faire de même en
l'ajoutant au groupe `docker`.

```bash
sudo usermod -aG docker sylvie
```

Attention, il faut se déconnecter et se reconnecter pour que l'ajout au
groupe fasse effet.

## Configuration

La configuration se fait via le fichier `src/test/resources/appwhite-tests-configuration.yaml`.

Il est possible de :
- Changer le nombre de répétitions de chaque test, chaque répétition donnant lieu à une ligne inscrite dans le fichier CSV résultat. On obtiendra donc des statistiques plus fiables.
- Indiquer si les tests doivent être exécutés sur des bases locales, des conteneurs jetables ou des instances PG ou MongoDB sur une infrastructure de prod
  - Dans ce dernier cas, préciser l'hôte, le port, l'identifiant, le mot de passe

Le fichier est auto-documenté (voir les commentaires et les exemples).

## Lancement

```bash
# Pour exécuter tous les tests
mvn test

# Pour exécuter seulement les tests ciblant une base de données vide
mvn test -Dtest="EmptyDatabaseTest"

# Pour exécuter seulement les tests ciblant une base de données avec le jeu de données RUA
mvn test -Dtest="RuaDatabaseTest"

# Pour exécuter un test spécifique, il faut ajouter à la classe, le nom de la méthode (préfixé de # et suivi de [*])
mvn test -Dtest="EmptyDatabaseTest#insertOneThousand[*]"
```

Voir la [documentation du paramètre `-Dtest` de la documentation Maven pour plus d'informations](https://maven.apache.org/surefire/maven-surefire-plugin/test-mojo.html#test).

À l'exécution, le journal indique où le fichier CSV sera généré (`target/surefire-workdir/appwhite.csv` par défaut).

## Développer sur le projet

Le projet est un projet Maven classique.

## Images Docker

Plusieurs images sont nécessaires pour Appwhite. Elles sont construites depuis la CI de ce projet.

![](docs/images_docker.png)

### Images Mongo et Postgres de base

 * `gitlab.mim-libre.fr:5050/menjs-dne-public/fr.edu.appwhite:mongo5.0_novolume`
 * `gitlab.mim-libre.fr:5050/menjs-dne-public/fr.edu.appwhite:postgres14_novolume`

Ces images sont quasiment identiques aux images officielles. La principale différence est qu'elles ne contiennent pas de directive `VOLUME` (il faut l'enlever pour pouvoir construire des images avec des données pré-chargées). Il y a aussi des adaptations pour faire fonctionner le build depuis le runner GitLab de ce projet, déployé dans un environnement restreint.

#### Mise à jour et construction depuis la CI

Pour une mise à jour mineure:

* Se rendre dans les dépôts des images officielles et récupérer le hash du commit le plus récent:
  * https://github.com/docker-library/postgres
  * https://github.com/docker-library/mongo
* Dans le fichier `.gitlab-ci.yml`, repérer les jobs `mongo5.0_novolume` et `postgres14_novolume`, mettre à jour `COMMIT_SHA` avec les nouvelles valeurs
* Lancer les jobs du stage `prebuild` (`mongo5.0_novolume` et `postgres14_novolume`)

Pour une mise à jour majeure, il faut également modifier la variable `GITHUB_PATH`. Sa valeur doit être le chemin du dossier contenant les sources de l'image souhaitée, par exemple `14/alpine` pour l'image `postgres:14-alpine`.

#### Construction à la main

* Récupérer le `Dockerfile` et le `docker-compose.sh` de l'image souhaitée (par exemple, pour l'image `postgres:14-alpine`, ces fichiers se trouvent [ici](https://github.com/docker-library/postgres/tree/master/14/alpine))
* Retirer du `Dockerfile` la ligne commençant par `VOLUME`
* `chmod +x docker-compose.sh`
* `docker build -t <tag_de_l'image> .`

### Image replica set

 * `gitlab.mim-libre.fr:5050/menjs-dne-public/fr.edu.appwhite:mongo5.0_replicaset`

Cette image fournit une [base Mongo standalone convertie en replica set](https://docs.mongodb.com/manual/tutorial/convert-standalone-to-replica-set/). Ceci est nécessaire pour pouvoir lancer des transactions.

#### Mise à jour et construction depuis la CI

Une fois que `mongo5.0_novolume` est à jour, lancer le job `mongo5.0_replicaset` du stage `build`

#### Construction à la main

Se placer dans le dossier `images/mongo` de ce projet, puis lancer `docker build -t gitlab.mim-libre.fr:5050/menjs-dne-public/fr.edu.appwhite:mongo5.0_replicaset .`

### Images avec des données préchargées

* `gitlab.mim-libre.fr:5050/menjs-dne-public/fr.edu.appwhite.bdd:mongo5.0_replicaset_with_rua_data`
* `gitlab.mim-libre.fr:5050/menjs-dne-public/fr.edu.appwhite.bdd:mongo5.0_with_rua_data`
* `gitlab.mim-libre.fr:5050/menjs-dne-public/fr.edu.appwhite.bdd:postgres14_with_rua_data`

Nous utilisons un export de données anonymisées du projet RUA. Il s'agit d'un fichier 7z contenant des exports BSON de 3 collections. Par précaution, même si les données sont anonymisées on ne les rend pas accessibles publiquement. Le fichier est présent uniquement sur la machine hébergeant le runner de ce projet. Les images avec les données préchargées ne sont pas poussées sur le registry de ce projet.



L'image `postgres14_with_rua_data` est construite à partir de `mongo5.0_with_rua_data`. Les données sont exportées depuis Mongo, transformées avec `jq` puis importées dans Postgres.

#### Mise à jour et construction depuis la CI

Une fois que `mongo5.0_novolume` est à jour, lancer les jobs `mongo5.0_with_rua_data` et `mongo5.0_replicaset_with_rua_data` du stage `build`. Une fois que `mongo5.0_with_rua_data` et `postgres14_novolume` sont à jour, lancer le job `postgres14_with_rua_data` du stage `build`.

#### Construction à la main

Il vous faut un fichier 7z contenant un dump d'une base de données Mongo (par exemple, un dump obtenu avec `mongodump`). Le fichier 7z doit contenir à la racine les fichiers `.bson`. Ces fichiers seront importés dans des collections ayant le même nom (sans l'extension).

* Se placer dans le dossier `images/mongo` de ce projet
* Dans les fichiers `Dockerfile.*`, remplacer `RUAV2_ano.7z` par le nouvel export
* Lancer le build des images Mongo:
  * `docker build -t gitlab.mim-libre.fr:5050/menjs-dne-public/fr.edu.appwhite.bdd:mongo5.0_replicaset_with_rua_data -f Dockerfile.mongo5.0_replicaset_with_rua_data .`
  * `docker build -t gitlab.mim-libre.fr:5050/menjs-dne-public/fr.edu.appwhite.bdd:mongo5.0_with_rua_data -f Dockerfile.mongo5.0_with_rua_data .`
* Se placer dans le dossier `images/postgres`
* Lancer le build de l'image Postgres: `docker build -t gitlab.mim-libre.fr:5050/menjs-dne-public/fr.edu.appwhite.bdd:postgres14_with_rua_data -f Dockerfile.postgres14_with_rua_data .`
