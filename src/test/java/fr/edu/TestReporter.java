package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 * 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * http://ec.europa.eu/idabc/eupl5
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * Le système de production du fichier CSV dans lequel on va consigner les mesures relevées pendant les tests.
 *
 * Cet objet sera injecté dans chaque test pour pouvoir y ajouter des lignes.
 */
public class TestReporter extends TestWatcher {

    private static final Logger logger = LogManager.getLogger(TestReporter.class);

    private static final File FILE;

    static {
        File javaIoTmpDir = SystemUtils.getJavaIoTmpDir();
        Preconditions.checkState(javaIoTmpDir.exists() || javaIoTmpDir.mkdirs());
        Preconditions.checkState(javaIoTmpDir.canWrite());
        FILE = new File(javaIoTmpDir, "appwhite.csv");
        logger.info("va enregistrer les mesures dans " + FILE.getAbsolutePath());
        boolean writeHeader = !FILE.exists();
        if (writeHeader) {
            List<String> header = List.of("Départ", "Cible", "Environnement", "Scénario", "Mesure", "Valeur");
            writeRecord(header);
        }
    }

    private final BenchmarkTarget benchmarkTarget;

    private final InitialDatabaseState initialDatabaseState;

    private final String databaseEnvironment;

    private String testDisplayName;

    public TestReporter(InitialDatabaseState initialDatabaseState, BenchmarkTarget benchmarkTarget, String databaseEnvironment) {
        this.initialDatabaseState = initialDatabaseState;
        this.benchmarkTarget = benchmarkTarget;
        this.databaseEnvironment = databaseEnvironment;
    }

    @Override
    protected void starting(Description description) {
        testDisplayName = description.getAnnotation(AppWhiteTest.class).displayName();
    }

    public void record(String measure, Number value) {
        List<String> record = List.of(
                initialDatabaseState.getDisplayName(),
                benchmarkTarget.getDisplayName(),
                databaseEnvironment,
                testDisplayName,
                measure,
                String.valueOf(value));
        writeRecord(record);
    }

    public void record(String measure, Instant from) {
        Instant to = Instant.now();
        record(measure, from, to);
    }

    public void record(String measure, Instant from, Instant to) {
        long milliseconds = ChronoUnit.MILLIS.between(from, to);
        record(measure + " (millisecondes)", milliseconds);
    }

    private static void writeRecord(List<String> record) {
        try (FileWriter fileWriter = new FileWriter(FILE, StandardCharsets.UTF_8, true)) {
            try (CSVPrinter csvPrinter = new CSVPrinter(fileWriter, CSVFormat.DEFAULT.withDelimiter(';'))) {
                csvPrinter.printRecord(record);
            }
        } catch (IOException e) {
            throw new RuntimeException("impossible d'écrire le fichier CSV " + FILE, e);
        }
    }
}
