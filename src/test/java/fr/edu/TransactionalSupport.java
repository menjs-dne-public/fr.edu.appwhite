package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 * 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * http://ec.europa.eu/idabc/eupl5
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

/**
 * Indique un niveau de support de la transaction (avec ou sans)
 */
public enum TransactionalSupport implements WithDisplayName {

    /**
     * JDBC sans autoCommit pour PG, mode replicatSet pour mongoDB
     */
    WITH_TRANSACTION_SUPPORT("Avec gestion de la transaction"),

    /**
     * JDBC avec autoCommit pour PG, MongoDB en mode standalone
     */
    WITHOUT_TRANSACTION_SUPPORT("Sans gestion de la transaction");

    private final String displayName;

    TransactionalSupport(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }
}
