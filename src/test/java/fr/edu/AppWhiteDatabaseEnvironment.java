package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 * 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * http://ec.europa.eu/idabc/eupl5
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

/**
 * Désigne l'environnement sur lequel on veut lancer les tests.
 */
public enum AppWhiteDatabaseEnvironment implements WithDisplayName {

    /**
     * Les tests seront exécutés sur les bases lancées via le docker-compose du projet
     */
    LOCAL("Environnement local"),

    /**
     * Utilisation de test container pour un conteneur spécifique au test
     */
    DISPOSABLE("Conteneur Docker à usage unique"),

    /**
     * Instance dont les coordonnées auront été renseignées dans {@link AppWhiteTestsConfiguration}
     */
    CUSTOM("Instance selon configuration");

    private final String displayName;

    AppWhiteDatabaseEnvironment(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }
}
