package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 * 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * http://ec.europa.eu/idabc/eupl5
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.wait.strategy.LogMessageWaitStrategy;
import org.testcontainers.shaded.com.github.dockerjava.core.DefaultDockerClientConfig;
import org.testcontainers.shaded.com.github.dockerjava.core.DockerClientConfig;
import org.testcontainers.shaded.com.github.dockerjava.core.DockerClientImpl;
import org.testcontainers.utility.DockerImageName;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.transport.DockerHttpClient;
import com.github.dockerjava.zerodep.ZerodepDockerHttpClient;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Gestion de tout ce qu'il faut préparer avant l'exécution de chaque test.
 *
 * <ul>
 *     <li>Lire la configuration utilisateur</li>
 *     <li>Prendre en compte le nombre de répétition</li>
 *     <li>Choisir l'environnement {@link AppWhiteDatabaseEnvironment} et calculer les cibles {@link BenchmarkTarget}</li>
 *     <li>Démarrer, configurer et et arrêter les conteneurs, s'ils sont utilisés</li>
 * </ul>
 */
public class MyTestTemplateInvocationContextProvider {

    private static final Logger logger = LogManager.getLogger(MyTestTemplateInvocationContextProvider.class);

    private static final AppWhiteTestsConfiguration CONFIGURATION =
            AppWhiteTestsConfiguration.INSTANCE;

    /**
     * Collection dont chaque élément représente une exécution à faire de chaque test.
     *
     * Chaque test devra être exécuter une fois pour chaque élément de la collection afin de faire la comparaison.
     *
     * Par exemple, si on a 5 répétition et 2 cibles (Mongo et Postgres), on aura 10 éléments.
     */
    public List<BenchmarkTarget> getRepeatedBenchmarkTargets() {
        List<BenchmarkTarget> repeatedContexts =
                Stream.generate(this::newRepetition)
                        .limit(CONFIGURATION.getRepetition())
                        .flatMap(Collection::stream)
                        .collect(Collectors.toUnmodifiableList());
        return repeatedContexts;
    }

    /**
     * Pour une seule répétition, on retourne l'ensemble des cibles sur lesquels chaque test doit être exécutér.
     */
    private Set<BenchmarkTarget> newRepetition() {
        final AppWhiteDatabaseEnvironment databaseEnvironment = CONFIGURATION.getDatabaseEnvironment();
        final Set<BenchmarkTarget> benchmarkTargets;
        if (databaseEnvironment == AppWhiteDatabaseEnvironment.CUSTOM) {
            benchmarkTargets = Set.of(CONFIGURATION.getCustomBenchmarkTarget());
        } else if (databaseEnvironment == AppWhiteDatabaseEnvironment.LOCAL) {
            benchmarkTargets = Set.of(BenchmarkTarget.MONGODB_STANDALONE, BenchmarkTarget.POSTGRESQL_WITHOUT_AUTOCOMMIT);
        } else if (databaseEnvironment == AppWhiteDatabaseEnvironment.DISPOSABLE) {
            benchmarkTargets = BenchmarkTarget.ALL;
        } else {
            throw new IllegalArgumentException("databaseEnvironment = " + databaseEnvironment);
        }
        return benchmarkTargets;
    }

    /**
     * Enregistre le paramètre pour fermeture à la coupure de la JVM
     *
     * On s'assure ainsi que quoi qu'il arrive (erreur pendant les tests), tout est bien fermé.
     */
    private void closeAtShutdown(SomeDatabaseOperation db) {
        Thread thread = new Thread(db::close);
        Runtime.getRuntime().addShutdownHook(thread);
    }

    /**
     * Trouver un port ouvert et libre sur la machine locale.
     */
    private static int findFreePort() {
        try (ServerSocket socket = new ServerSocket(0)) {
            socket.setReuseAddress(true);
            int port = socket.getLocalPort();
            try {
                socket.close();
            } catch (IOException ignored) {
                // Ignore IOException on close()
            }
            return port;
        } catch (IOException ignored) {
            // Ignore IOException on open
        }
        // Ignore IOException on close()
        throw new IllegalStateException("ne peut pas trouver un port libre");
    }

    /**
     * Crée l'objet {@link SomeDatabaseOperation} qui va être injecté dans le test.
     */
    public SomeDatabaseOperation newTestTemplateInvocationContext(InitialDatabaseState initialDatabaseState, BenchmarkTarget benchmarkTarget) throws ImageNotAvailableException {
        final SomeDatabaseOperation db;
        final AppWhiteDatabaseEnvironment databaseEnvironment = CONFIGURATION.getDatabaseEnvironment();
        if (databaseEnvironment == AppWhiteDatabaseEnvironment.CUSTOM) {
            db = newCustomDb(benchmarkTarget);
        } else if (databaseEnvironment == AppWhiteDatabaseEnvironment.LOCAL) {
            db = newLocalDb(benchmarkTarget);
        } else if (databaseEnvironment == AppWhiteDatabaseEnvironment.DISPOSABLE) {
            db = newDisposableDb(initialDatabaseState, benchmarkTarget);
        } else {
            throw new IllegalArgumentException("databaseEnvironment = " + databaseEnvironment);
        }
        return db;
    }

    /**
     * Créer un {@link SomeDatabaseOperation} pour accéder à une base donnée démarrée en local.
     */
    private SomeDatabaseOperation newLocalDb(BenchmarkTarget benchmarkTarget) {
        final SomeDatabaseOperation db;
        if (benchmarkTarget.getDatabase().isMongoDb()) {
            db = SomeDatabaseOperationWithMongoDb.fromCredentials(
                    "localhost",
                    "27017",
                    benchmarkTarget.getTransactionalSupport() == TransactionalSupport.WITH_TRANSACTION_SUPPORT);
        } else if (benchmarkTarget.getDatabase().isPostgresql()) {
            db = SomeDatabaseOperationWithPostgresql.forCredentials(
                    "localhost",
                    "5432",
                    "appwhite",
                    "appwhite",
                    "appwhite",
                    benchmarkTarget.getTransactionalSupport() == TransactionalSupport.WITHOUT_TRANSACTION_SUPPORT);
        } else {
            throw new IllegalStateException("non géré " + benchmarkTarget.getDatabase());
        }
        return db;
    }

    /**
     * Créer un {@link SomeDatabaseOperation} pour accéder à une base donnée sur l'environnement configuré par l'utilisateur.
     */
    private SomeDatabaseOperation newCustomDb(BenchmarkTarget benchmarkTarget) {
        final SomeDatabaseOperation db;
        if (benchmarkTarget.getDatabase().isMongoDb()) {
            db = SomeDatabaseOperationWithMongoDb.fromCredentials(
                    CONFIGURATION.getCustomHost(),
                    CONFIGURATION.getCustomPort(),
                    CONFIGURATION.getCustomUser(),
                    CONFIGURATION.getCustomPassword(),
                    benchmarkTarget.getTransactionalSupport() == TransactionalSupport.WITH_TRANSACTION_SUPPORT);
        } else if (benchmarkTarget.getDatabase().isPostgresql()) {
            db = SomeDatabaseOperationWithPostgresql.forCredentials(
                    CONFIGURATION.getCustomHost(),
                    CONFIGURATION.getCustomPort(),
                    CONFIGURATION.getCustomDatabaseName(),
                    CONFIGURATION.getCustomUser(),
                    CONFIGURATION.getCustomPassword(),
                    benchmarkTarget.getTransactionalSupport() == TransactionalSupport.WITHOUT_TRANSACTION_SUPPORT);
        } else {
            throw new IllegalStateException("non géré " + benchmarkTarget.getDatabase());
        }
        return db;
    }

     /**
     * Créer un {@link SomeDatabaseOperation} pour accéder à une base donnée dans un conteneur jetable.
     */
    private SomeDatabaseOperation newDisposableDb(InitialDatabaseState initialDatabaseState, BenchmarkTarget benchmarkTarget) throws ImageNotAvailableException {
        final SomeDatabaseOperation db;
        if (benchmarkTarget.getDatabase().isMongoDb()) {
            final DockerImageName dockerImageName;
            if (initialDatabaseState == InitialDatabaseState.EMPTY) {
                if (benchmarkTarget.getTransactionalSupport() == TransactionalSupport.WITH_TRANSACTION_SUPPORT) {
                    dockerImageName = getDockerImageNameIfAvailable("gitlab.mim-libre.fr:5050/menjs-dne-public/fr.edu.appwhite:mongo5.0_replicaset");
                } else if (benchmarkTarget.getTransactionalSupport() == TransactionalSupport.WITHOUT_TRANSACTION_SUPPORT) {
                    dockerImageName = getDockerImageNameIfAvailable("mongo:5.0");
                } else {
                    throw new IllegalArgumentException("ne sait pas géré " + benchmarkTarget.getTransactionalSupport() + initialDatabaseState);
                }
            } else if (initialDatabaseState == InitialDatabaseState.RUA) {
                if (benchmarkTarget.getTransactionalSupport() == TransactionalSupport.WITH_TRANSACTION_SUPPORT) {
                    dockerImageName = getDockerImageNameIfAvailable("gitlab.mim-libre.fr:5050/menjs-dne-public/fr.edu.appwhite:mongo5.0_replicaset_with_rua_data");
                } else if (benchmarkTarget.getTransactionalSupport() == TransactionalSupport.WITHOUT_TRANSACTION_SUPPORT) {
                    dockerImageName = getDockerImageNameIfAvailable("gitlab.mim-libre.fr:5050/menjs-dne-public/fr.edu.appwhite:mongo5.0_with_rua_data");
                } else {
                    throw new IllegalArgumentException("ne sait pas géré " + benchmarkTarget.getTransactionalSupport() + initialDatabaseState);
                }
            } else {
                throw new IllegalArgumentException("non géré " + initialDatabaseState);
            }
            db = newMongoDb(dockerImageName, benchmarkTarget.getTransactionalSupport());
        } else if (benchmarkTarget.getDatabase().isPostgresql()) {
            db = newPostgreSql(initialDatabaseState);
        } else {
            throw new IllegalStateException("non géré " + benchmarkTarget.getDatabase());
        }
        closeAtShutdown(db);
        return db;
    }

    /**
     * Créer un {@link SomeDatabaseOperation} en démarrant un conteneur PG.
     */
    private SomeDatabaseOperation newPostgreSql(InitialDatabaseState initialDatabaseState) throws ImageNotAvailableException {
        int port = findFreePort();
        if (logger.isDebugEnabled()) {
            logger.debug("démarrage d'un postgresql embarqué sur le port " + port);
        }
        String user = "appwhite";
        String password = "appwhite";
        DockerImageName dockerImageName;
        PostgreSQLContainer postgreSQLContainer;
        if (initialDatabaseState == InitialDatabaseState.EMPTY) {
            dockerImageName = getDockerImageNameIfAvailable("postgres:14-alpine");
            postgreSQLContainer = new PostgreSQLContainer<>(dockerImageName);
        } else if (initialDatabaseState == InitialDatabaseState.RUA) {
            dockerImageName = getDockerImageNameIfAvailable("gitlab.mim-libre.fr:5050/menjs-dne-public/fr.edu.appwhite:postgres14_with_rua_data").asCompatibleSubstituteFor("postgres");
            postgreSQLContainer = new PostgreSQLContainerWithFixedWaitingStrategy(dockerImageName);
        } else {
            throw new IllegalArgumentException("non géré " + initialDatabaseState);
        }
        postgreSQLContainer
                .withDatabaseName("appwhite")
                .withUsername(user)
                .withPassword(password);
        postgreSQLContainer.setPortBindings(ImmutableList.of(port + ":" + PostgreSQLContainer.POSTGRESQL_PORT));
        final String command = getDisposablePostgreSqlCommand();
        postgreSQLContainer.withCommand(command);
        postgreSQLContainer.start();
        SomeDatabaseOperation db = SomeDatabaseOperationWithPostgresql.forUrl(
                postgreSQLContainer.getJdbcUrl(),
                user,
                password,
                postgreSQLContainer, false);
        return db;
    }

    private String getDisposablePostgreSqlCommand() {
        Map<String, String> configurationForDisposablePostgreSql = CONFIGURATION.getConfigurationForDisposablePostgreSql();
        final String command;
        if (configurationForDisposablePostgreSql.isEmpty()) {
            // pas de configuration particulière, on utilise le conteneur tel quel
            command = "postgres";
        } else {
            final String commandArguments = configurationForDisposablePostgreSql.entrySet().stream()
                    .map(entry -> entry.getKey() + "=" + entry.getValue())
                    .map(postgresqlDotConfDirective -> "-c " + postgresqlDotConfDirective)
                    .collect(Collectors.joining(" "));
            command = "postgres " + commandArguments;
        }
        return command;
    }

    public String getDatabaseEnvironmentDescription(Database database) {
        final AppWhiteDatabaseEnvironment databaseEnvironment = CONFIGURATION.getDatabaseEnvironment();
        Map<String, String> configurationForDisposablePostgreSql = CONFIGURATION.getConfigurationForDisposablePostgreSql();
        final String databaseEnvironmentDescription;
        final boolean disposablePostgreSqlWithCustomConfiguration = databaseEnvironment == AppWhiteDatabaseEnvironment.DISPOSABLE
                                                                 && database == Database.POSTGRESQL_14
                                                                 && !configurationForDisposablePostgreSql.isEmpty()
                                                                  ;
        if (disposablePostgreSqlWithCustomConfiguration) {
            final String configuration = Joiner.on(" ")
                    .withKeyValueSeparator("=")
                    .join(configurationForDisposablePostgreSql);
            databaseEnvironmentDescription = databaseEnvironment.getDisplayName() + " " + configuration;
        } else {
            databaseEnvironmentDescription = databaseEnvironment.getDisplayName();
        }
        return databaseEnvironmentDescription;
    }

    /**
     * Contournement d'un problème dans {@link PostgreSQLContainer}.
     *
     * Il attend par défaut que "database system is ready to accept connections" s'affiche
     * deux fois. C'est le cas dans l'image par défaut mais dans l'image avec des données,
     * la base n'est démarrée qu'une seule fois donc on n'attend le message qu'une seule fois
     * plutôt que d'attendre à l'infini. La code a simplement été dupliqué.
     */
    private static class PostgreSQLContainerWithFixedWaitingStrategy extends PostgreSQLContainer {
        private PostgreSQLContainerWithFixedWaitingStrategy(DockerImageName dockerImageName) {
            super(dockerImageName);
            this.waitStrategy = ((LogMessageWaitStrategy) getWaitStrategy()).withTimes(1);
        }
    }

    /**
     * Créer un {@link SomeDatabaseOperation} en démarrant un conteneur MongoDB.
     */
    private SomeDatabaseOperation newMongoDb(DockerImageName dockerImageName, TransactionalSupport transactionalSupport) {
        GenericContainer mongoDBContainer = new GenericContainer<>(dockerImageName)
                .withExposedPorts(27017);
        mongoDBContainer.start();
        logger.info("a démarré " + mongoDBContainer.getContainerName());
        String host = mongoDBContainer.getHost();
        String port = String.valueOf(mongoDBContainer.getFirstMappedPort());
        String url = "mongodb://" + host + ":" + port + "/appwhite";
        boolean transactionSupported = transactionalSupport == TransactionalSupport.WITH_TRANSACTION_SUPPORT;
        SomeDatabaseOperation db = SomeDatabaseOperationWithMongoDb.fromUrl(url, mongoDBContainer, transactionSupported);
        return db;
    }

    /**
     * Contrôle que l'image passée en paramètre est bien disponible.
     */
    private DockerImageName getDockerImageNameIfAvailable(String dockerImageName) throws ImageNotAvailableException {
        boolean imageIsCustom = dockerImageName.contains("gitlab.mim-libre.fr:5050");
        if (imageIsCustom) {
            DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder().build();
            DockerHttpClient httpClient = new ZerodepDockerHttpClient.Builder()
                    .dockerHost(config.getDockerHost())
                    .sslConfig(config.getSSLConfig())
                    .build();
            DockerClient dockerClient = DockerClientImpl.getInstance(config, httpClient);
            List<Image> images = dockerClient.listImagesCmd().exec();
            logger.debug("On cherche " + dockerImageName + " dans la liste des images:");
            for (Image image : images) {
                String[] tags = image.getRepoTags();
                // La methode getRepoTags() peut retourner null. Ça arrive lorsque une image a un digest et <none> en tag
                if (tags != null) {
                    List<String> tagsList = Arrays.asList(tags);
                    logger.debug(tagsList);
                    if (tagsList.contains(dockerImageName)) {
                        logger.info("va lancer avec l'image privée " + dockerImageName);
                        return DockerImageName.parse(dockerImageName);
                    }
                }
            }
            throw new ImageNotAvailableException(String.format("L'image privée '%s' n'est pas disponible. Veuillez la télécharger.", dockerImageName), dockerImageName);
        } else {
            logger.info("va lancer avec l'image publique " + dockerImageName);
            return DockerImageName.parse(dockerImageName);
        }
    }

    /**
     * Exception levée si une image Docker n'est pas disponible.
     */
    public static class ImageNotAvailableException extends Exception {

        private final String dockerImageName;

        public ImageNotAvailableException(String message, String dockerImageName) {
            super(message);
            this.dockerImageName = dockerImageName;
        }

        public String getDockerImageName() {
            return dockerImageName;
        }
    }
}
