package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 * 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * http://ec.europa.eu/idabc/eupl5
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Set;

/**
 * Représente les différentes cibles de la comparaison.
 */
public enum BenchmarkTarget implements WithDisplayName {

    /**
     * PostreSQL avec JDBC en mode autoCommit
     */
    POSTGRESQL_WITH_AUTOCOMMIT(Database.POSTGRESQL_14, TransactionalSupport.WITHOUT_TRANSACTION_SUPPORT, Database.POSTGRESQL_14.getDisplayName() + " autoCommit activé"),

    /**
     * PostgreSQL avec autoCommit désactivé (commit manuel)
     */
    POSTGRESQL_WITHOUT_AUTOCOMMIT(Database.POSTGRESQL_14, TransactionalSupport.WITH_TRANSACTION_SUPPORT, Database.POSTGRESQL_14.getDisplayName() + " autoCommit désactivé, commit manuel"),

    /**
     * MongoDB en mode "standalone" (donc non-transactionnel)
     */
    MONGODB_STANDALONE(Database.MONGODB_5, TransactionalSupport.WITHOUT_TRANSACTION_SUPPORT, Database.MONGODB_5.getDisplayName() + " en mode standalone (non-transactionnel)"),

    /**
     * MongoDB en mode replicat-set (transactionnel)
     */
    MONGODB_REPLICASET(Database.MONGODB_5, TransactionalSupport.WITH_TRANSACTION_SUPPORT, Database.MONGODB_5.getDisplayName() + " en mode replicat-set (transactionnel)");

    /**
     * Toutes les cibles possibles.
     */
    public static final Set<BenchmarkTarget> ALL = Set.of(
            POSTGRESQL_WITH_AUTOCOMMIT,
            POSTGRESQL_WITHOUT_AUTOCOMMIT,
            MONGODB_STANDALONE,
            MONGODB_REPLICASET
    );

    private final Database database;

    private final TransactionalSupport transactionalSupport;

    private final String displayName;

    BenchmarkTarget(Database database, TransactionalSupport transactionalSupport, String displayName) {
        this.database = database;
        this.transactionalSupport = transactionalSupport;
        this.displayName = displayName;
    }

    public Database getDatabase() {
        return database;
    }

    public TransactionalSupport getTransactionalSupport() {
        return transactionalSupport;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("database", database)
                .append("transactionalSupport", transactionalSupport)
                .append("displayName", displayName)
                .toString();
    }
}
