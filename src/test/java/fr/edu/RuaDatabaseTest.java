package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 * 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * http://ec.europa.eu/idabc/eupl5
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

import com.mongodb.assertions.Assertions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * L'ensemble des tests qu'on va passer sur une base de données vide au lancement {@link InitialDatabaseState#RUA}
 */
@RunWith(Parameterized.class)
public class RuaDatabaseTest {

    private static final Logger logger = LogManager.getLogger(RuaDatabaseTest.class);

    private static final MyTestTemplateInvocationContextProvider MY_TEST_TEMPLATE_INVOCATION_CONTEXT_PROVIDER = new MyTestTemplateInvocationContextProvider();

    private final BenchmarkTarget benchmarkTarget;

    @Rule
    public final TestReporter testReporter;

    private SomeDatabaseOperation someDatabaseOperation;

    @Parameterized.Parameters(name = "{1}")
    public static Collection<Object[]> data() {
        return MY_TEST_TEMPLATE_INVOCATION_CONTEXT_PROVIDER.getRepeatedBenchmarkTargets().stream()
                .map(benchmarkTarget -> new Object[]{benchmarkTarget, benchmarkTarget.getDisplayName()})
                .collect(Collectors.toUnmodifiableList());
    }

    public RuaDatabaseTest(BenchmarkTarget benchmarkTarget, String ignoredBenchmarkTargetDisplayName) {
        this.benchmarkTarget = benchmarkTarget;
        this.testReporter = new TestReporter(InitialDatabaseState.RUA, benchmarkTarget, MY_TEST_TEMPLATE_INVOCATION_CONTEXT_PROVIDER.getDatabaseEnvironmentDescription(benchmarkTarget.getDatabase()));
    }

    @Before
    public void openResources() {
        try {
            someDatabaseOperation = MY_TEST_TEMPLATE_INVOCATION_CONTEXT_PROVIDER.newTestTemplateInvocationContext(InitialDatabaseState.RUA, benchmarkTarget);
        } catch (MyTestTemplateInvocationContextProvider.ImageNotAvailableException e) {
            Assume.assumeNoException("image non disponible " + e.getDockerImageName(), e);
        }
    }

    @After
    public void closeResources() {
        if (someDatabaseOperation != null) {
            someDatabaseOperation.close();
        }
    }

    @Test
    @AppWhiteTest(displayName = "Dénombrement des documents")
    public void count() {
        {
            final Instant begin = Instant.now();
            int actualCount = someDatabaseOperation.count();
            testReporter.record("Temps nécessaire à froid", begin);
            Assert.assertEquals(TableReference.agents().getExpectedCount(), actualCount);
        }
        {
            someDatabaseOperation.insert(List.of(TableReference.agents().getSample()));
            someDatabaseOperation.commit();
        }
        {
            final Instant begin = Instant.now();
            int actualCount = someDatabaseOperation.count();
            testReporter.record("Temps nécessaire à chaud", begin);
            Assert.assertEquals(TableReference.agents().getExpectedCount() + 1, actualCount);
        }
    }

    @Test
    @AppWhiteTest(displayName = "Charger un document par son identifiant")
    public void findById() {
        final Set<String> agentIds = TableReference.agents().getSampleIds();
        final Instant begin = Instant.now();
        agentIds.stream()
                .map(someDatabaseOperation::findAgentById)
                .forEach(Assertions::assertNotNull);
        testReporter.record("Temps pour charger " + agentIds.size() + " documents", begin);
    }

    @Test
    @AppWhiteTest(displayName = "Charger plusieurs documents par leurs identifiants")
    public void findByIds() {
        final Set<String> agentIds = TableReference.agents().getSampleIds();
        final Instant begin = Instant.now();
        final List<Agent> agents = someDatabaseOperation.findAgentsByIds(agentIds);
        Assert.assertEquals(agentIds.size(), agents.size());
        testReporter.record("Temps pour charger " + agentIds.size() + " documents", begin);
    }

    private final String dateIso = "2021-09-19T22:00:00Z";

    private final Set<String> nirs = TableReference.agents().getSampleNirs().stream()
            .limit(100)
            .collect(Collectors.toUnmodifiableSet());

    private final Set<String> nirs2 = Set.of(
            "173039941605234",
            "177074900713454",
            "160127511217383",
            "275085935201593",
            "179106938402005",
            "171032628103460",
            "175024918002337",
            "257037511442515",
            "162077500216611",
            "259014933300167",
            "157117500623831",
            "266082209332569",
            "156104523417019",
            "259072505609209",
            "277079722940918",
            "273046613610523",
            "156049712824572",
            "191075145473745",
            "184025123001686",
            "178088512802092",
            "282056938304128",
            "181058400723739",
            "170087812601262",
            "173021404702425",
            "183071820505670",
            "177039202411485",
            "187125050202310",
            "172042432212818",
            "169040128313905",
            "178059117406806",
            "275029922302066",
            "187074221824802",
            "158053155511850",
            "282129202604837",
            "182045145400209",
            "176079913437735",
            "262017055003881",
            "165081305508545",
            "262037864628073",
            "267057511526278",
            "268078306909435",
            "278095819408305",
            "168033726129555",
            "283095438206764",
            "187047511461372",
            "170109934109971"
    );

    private final Set<String> gradeCodes = Set.of("G04299", "G04296", "G04297", "G04377", "G03874", "G04376", "G04298", "G04302");

    @Test
    @AppWhiteTest(displayName = "Recherche agent selon NIR")
    public void searchAgent() {
        {
            final Instant begin = Instant.now();
            final Set<Agent> agents = searchAgentsByNirs();
            testReporter.record("Temps pour charger un par un les " + agents.size() + " documents pour chacun des " + nirs.size() + " codes NIR, sans index", begin);
        }
        someDatabaseOperation.indexAgentNirsUsingHash();
        {
            final Instant begin = Instant.now();
            final Set<Agent> agents = searchAgentsByNirs();
            testReporter.record("Temps pour charger un par un les " + agents.size() + " documents pour chacun des " + nirs.size() + " codes NIR, avec index hash", begin);
        }
    }

    @Test
    @AppWhiteTest(displayName = "Recherche agent selon 'dateFin'")
    public void searchAgents() {
        {
            final Instant begin = Instant.now();
            doSearchAgentsByDateFin();
            testReporter.record("Temps nécessaire à froid, sans index", begin);
        }
        {
            final Instant begin = Instant.now();
            doSearchAgentsByDateFin();
            testReporter.record("Temps nécessaire à chaud, sans index", begin);
        }
        {
            final Instant begin = Instant.now();
            someDatabaseOperation.indexAgentsDateFin();
            testReporter.record("Temps nécessaire pour ajouter l'index sur dateFin", begin);
        }
        {
            final Instant begin = Instant.now();
            doSearchAgentsByDateFin();
            testReporter.record("Temps nécessaire à chaud, avec index", begin);
        }
    }

    @Test
    @AppWhiteTest(displayName = "Recherche agent selon 'dateDebut', 'dateFin' et 'nir'")
    public void searchAgents2() {
        {
            final Instant begin = Instant.now();
            doSearchAgentByDateNirs();
            testReporter.record("Temps nécessaire à froid, sans index", begin);
        }
        {
            final Instant begin = Instant.now();
            doSearchAgentByDateNirs();
            testReporter.record("Temps nécessaire à chaud, sans index", begin);
        }
        {
            final Instant begin = Instant.now();
            someDatabaseOperation.indexAgentsDateFinDateDebutNir();
            testReporter.record("Temps nécessaire pour ajouter l'index sur 'dateDebut', 'dateFin' et 'nir'", begin);
        }
        {
            final Instant begin = Instant.now();
            doSearchAgentByDateNirs();
            testReporter.record("Temps nécessaire à chaud, avec index", begin);
        }
    }

    @Test
    @AppWhiteTest(displayName = "Recherche agent si un élément de carrière à un grade dont le code est recherché")
    public void searchAgents3() {
        {
            final Instant begin = Instant.now();
            doSearchAgentByCarriereGradeCodes();
            testReporter.record("Temps nécessaire à froid, sans index", begin);
        }
        {
            final Instant begin = Instant.now();
            doSearchAgentByCarriereGradeCodes();
            testReporter.record("Temps nécessaire à chaud, sans index", begin);
        }
        {
            final Instant begin = Instant.now();
            someDatabaseOperation.indexAgentsCarriereGradeCodes();
            testReporter.record("Temps nécessaire pour ajouter l'index", begin);
        }
        {
            final Instant begin = Instant.now();
            doSearchAgentByCarriereGradeCodes();
            testReporter.record("Temps nécessaire à chaud, avec index", begin);
        }
    }

    private Set<Agent> searchAgentsByNirs() {
        final Set<Agent> agents = nirs.stream()
                .map(someDatabaseOperation::findAgentByNir)
                .flatMap(Collection::stream)
                .collect(Collectors.toUnmodifiableSet());
        agents.forEach(Assertions::assertNotNull);
        Assert.assertEquals(1764, agents.size());
        return agents;
    }

    private void doSearchAgentsByDateFin() {
        final List<Agent> agents = someDatabaseOperation.searchAgents(dateIso);
        Assert.assertEquals(8954, agents.size());
    }

    private void doSearchAgentByDateNirs() {
        final List<Agent> agents = someDatabaseOperation.searchAgents(dateIso, nirs2, gradeCodes);
        Assert.assertEquals(31, agents.size());
    }

    private void doSearchAgentByCarriereGradeCodes() {
        final List<Agent> agents = someDatabaseOperation.searchAgents(gradeCodes);
        Assert.assertEquals(22859, agents.size());
    }

    @Test
    @AppWhiteTest(displayName = "Parcourir tous les documents de la base")
    public void forEach() {
        final Instant begin = Instant.now();
        someDatabaseOperation.forEachAgent(Assert::assertNotNull);
        testReporter.record("Temps nécessaire", begin);
    }

    @Test
    @AppWhiteTest(displayName = "Sommer la valeur d'un document sur toute la base")
    public void sum() {
        final Instant begin = Instant.now();
        final int expectedSum = someDatabaseOperation.sumNombreConges();
        Assert.assertEquals(24016, expectedSum);
        testReporter.record("Temps nécessaire", begin);
    }

    @Test
    @AppWhiteTest(displayName = "Calculer le nombre d'agent par département de naissance")
    public void countGroupBy() {
        final Instant begin = Instant.now();
        Map<String, Integer> actual = someDatabaseOperation.countAgentPerDepartementNaissance();
        Assert.assertEquals(106, actual.size());
        Assert.assertEquals(TableReference.agents().getExpectedCount(), actual.values().stream().mapToInt(Integer::intValue).sum());
        testReporter.record("Temps nécessaire", begin);
    }
}
