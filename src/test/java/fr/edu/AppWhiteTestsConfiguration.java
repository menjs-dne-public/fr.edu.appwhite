package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 * 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * http://ec.europa.eu/idabc/eupl5
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Représente la configuration par l'utilisateur des tests via le fichier de configuration dédié {@link #APPWHITE_TESTS_CONFIGURATION_YAML_PATH}.
 */
public class AppWhiteTestsConfiguration {

    public static final AppWhiteTestsConfiguration INSTANCE;

    private static final String APPWHITE_TESTS_CONFIGURATION_YAML_PATH = "/appwhite-tests-configuration.yaml";

    static {
        try (InputStream yaml = AppWhiteTestsConfiguration.class.getResourceAsStream(APPWHITE_TESTS_CONFIGURATION_YAML_PATH)) {
            YAMLMapper mapper = new YAMLMapper();
            INSTANCE = mapper.readValue(yaml, AppWhiteTestsConfiguration.class);
        } catch (IOException e) {
            throw new RuntimeException("incapable de lire le fichier de configuration des tests", e);
        }
    }

    /**
     * Le nombre de fois où on va répéter chaque test à l'identique.
     */
    private int repetition = 1;

    private AppWhiteDatabaseEnvironment databaseEnvironment = AppWhiteDatabaseEnvironment.DISPOSABLE;

    private Map<String, String> configurationForDisposablePostgreSql = new LinkedHashMap<>();

    private BenchmarkTarget customBenchmarkTarget;

    private String customHost;

    private String customPort;

    private String customUser;

    private String customPassword;

    private String customDatabaseName;

    public int getRepetition() {
        return repetition;
    }

    public void setRepetition(int repetition) {
        this.repetition = repetition;
    }

    public AppWhiteDatabaseEnvironment getDatabaseEnvironment() {
        return databaseEnvironment;
    }

    public void setDatabaseEnvironment(AppWhiteDatabaseEnvironment databaseEnvironment) {
        this.databaseEnvironment = databaseEnvironment;
    }

    public String getCustomHost() {
        return customHost;
    }

    public void setCustomHost(String customHost) {
        this.customHost = customHost;
    }

    public String getCustomPort() {
        return customPort;
    }

    public void setCustomPort(String customPort) {
        this.customPort = customPort;
    }

    public String getCustomUser() {
        return customUser;
    }

    public void setCustomUser(String customUser) {
        this.customUser = customUser;
    }

    public String getCustomPassword() {
        return customPassword;
    }

    public void setCustomPassword(String customPassword) {
        this.customPassword = customPassword;
    }

    public String getCustomDatabaseName() {
        return customDatabaseName;
    }

    public void setCustomDatabaseName(String customDatabaseName) {
        this.customDatabaseName = customDatabaseName;
    }

    public BenchmarkTarget getCustomBenchmarkTarget() {
        return customBenchmarkTarget;
    }

    public void setCustomBenchmarkTarget(BenchmarkTarget customBenchmarkTarget) {
        this.customBenchmarkTarget = customBenchmarkTarget;
    }

    public Map<String, String> getConfigurationForDisposablePostgreSql() {
        return configurationForDisposablePostgreSql;
    }

    public void setConfigurationForDisposablePostgreSql(Map<String, String> configurationForDisposablePostgreSql) {
        this.configurationForDisposablePostgreSql = configurationForDisposablePostgreSql;
    }
}
