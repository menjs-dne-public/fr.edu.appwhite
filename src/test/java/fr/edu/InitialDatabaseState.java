package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 * 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * http://ec.europa.eu/idabc/eupl5
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

/**
 * Les différents situations de départ pour une base de données de tests
 */
public enum InitialDatabaseState implements WithDisplayName {

    /**
     * Au départ, la base de données en vide
     */
    EMPTY("Départ base de données vide"),

    /**
     * Au départ, la base de données contient les données RUA
     */
    RUA("Départ base de données RUA");

    private final String displayName;

    InitialDatabaseState(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }
}
