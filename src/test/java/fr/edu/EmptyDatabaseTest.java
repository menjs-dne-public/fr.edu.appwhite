package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 * 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * http://ec.europa.eu/idabc/eupl5
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

import com.google.common.collect.Iterators;
import com.google.common.collect.UnmodifiableIterator;
import com.mongodb.MongoCommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * L'ensemble des tests qu'on va passer sur une base de données vide au lancement {@link InitialDatabaseState#EMPTY}
 */
@RunWith(Parameterized.class)
public class EmptyDatabaseTest {

    private static final Logger logger = LogManager.getLogger(EmptyDatabaseTest.class);

    private static final MyTestTemplateInvocationContextProvider MY_TEST_TEMPLATE_INVOCATION_CONTEXT_PROVIDER = new MyTestTemplateInvocationContextProvider();

    private final BenchmarkTarget benchmarkTarget;

    @Rule
    public final TestReporter testReporter;

    private SomeDatabaseOperation someDatabaseOperation;

    @Parameterized.Parameters(name = "{1}")
    public static Collection<Object[]> data() {
        return MY_TEST_TEMPLATE_INVOCATION_CONTEXT_PROVIDER.getRepeatedBenchmarkTargets().stream()
                .map(benchmarkTarget -> new Object[]{benchmarkTarget, benchmarkTarget.getDisplayName()})
                .collect(Collectors.toUnmodifiableList());
    }

    public EmptyDatabaseTest(BenchmarkTarget benchmarkTarget, String ignoredBenchmarkTargetDisplayName) {
        this.benchmarkTarget = benchmarkTarget;
        this.testReporter = new TestReporter(InitialDatabaseState.EMPTY, benchmarkTarget, MY_TEST_TEMPLATE_INVOCATION_CONTEXT_PROVIDER.getDatabaseEnvironmentDescription(benchmarkTarget.getDatabase()));
    }

    @Before
    public void openResources() {
        try {
            someDatabaseOperation = MY_TEST_TEMPLATE_INVOCATION_CONTEXT_PROVIDER.newTestTemplateInvocationContext(InitialDatabaseState.EMPTY, benchmarkTarget);
        } catch (MyTestTemplateInvocationContextProvider.ImageNotAvailableException e) {
            Assume.assumeNoException("image non disponible " + e.getDockerImageName(), e);
        }
    }

    @After
    public void closeResources() {
        if (someDatabaseOperation != null) {
            someDatabaseOperation.close();
        }
    }

    @Test
    @AppWhiteTest(displayName = "Insérer 1 millier de lignes")
    public void insertOneThousand() {
        insertMany(testReporter, someDatabaseOperation, 1000);
    }

    @Test
    @AppWhiteTest(displayName = "Insérer 10 milliers de lignes")
    public void insertTenThousands() {
        insertMany(testReporter, someDatabaseOperation, 10000);
    }

    @Test
    @AppWhiteTest(displayName = "Insérer 100 milliers de lignes")
    public void insertOnHundredThousands() {
        insertMany(testReporter, someDatabaseOperation, 100000);
    }

    @Test
    @AppWhiteTest(displayName = "Insérer 1 million de lignes")
    @Ignore("Java heap space à régler")
    public void insertOneMillion() {
        insertMany(testReporter, someDatabaseOperation, 1000000);
    }

    private void insertMany(TestReporter testReporter, SomeDatabaseOperation someDatabaseOperation, int count) {
        List<Agent> agents = Collections.nCopies(count, TableReference.agents().getSample());
        final Instant begin = Instant.now();
        someDatabaseOperation.insert(agents);
        someDatabaseOperation.commit();
        final Instant end = Instant.now();
        final int actualCount = someDatabaseOperation.count();
        Assert.assertEquals(count, actualCount);
        testReporter.record("Temps nécessaire", begin, end);
    }

    @Test
    @AppWhiteTest(displayName = "Insérer 30k lignes, par paquet 10 milliers, commit final")
    public void insertOneMillionPerTenThousandsBatchCommitFinally() {
        insertMany(testReporter, someDatabaseOperation, 30000, 10000, BatchCommitStrategy.FINALLY);
    }

    @Test
    @AppWhiteTest(displayName = "Insérer 30k lignes, par paquet 1 millier, commit final")
    public void insertOneMillionPerOneThousandBatchCommitFinally() {
        insertMany(testReporter, someDatabaseOperation, 30000, 1000, BatchCommitStrategy.FINALLY);
    }

    @Test
    @AppWhiteTest(displayName = "Insérer 30k lignes, par paquet 10 milliers, commit par lot")
    public void insertOneMillionPerTenThousandsBatchCommitPerBatch() {
        insertMany(testReporter, someDatabaseOperation, 30000, 10000, BatchCommitStrategy.PER_BATCH);
    }

    @Test
    @AppWhiteTest(displayName = "Insérer 30k lignes, par paquet 1 millier, commit par lot")
    public void insertOneMillionPerOneThousandBatchCommitPerBatch() {
        insertMany(testReporter, someDatabaseOperation, 30000, 1000, BatchCommitStrategy.PER_BATCH);
    }

    @Test
    @AppWhiteTest(displayName = "Insérer 30k lignes, par centaine")
    @Ignore("trop long et l'expiration de 2 minutes n'y met pas fin")
    public void insertOneMillionPerOneHundredsBatch() {
        insertMany(testReporter, someDatabaseOperation, 30000, 100, BatchCommitStrategy.FINALLY);
    }

    @Test
    @AppWhiteTest(displayName = "Insérer 30k lignes, par dizaine")
    @Ignore("trop long et l'expiration de 2 minutes n'y met pas fin")
    public void insertOneMillionPerTensBatch() {
        insertMany(testReporter, someDatabaseOperation, 30000, 10, BatchCommitStrategy.FINALLY);
    }

    private void insertMany(TestReporter testReporter, SomeDatabaseOperation someDatabaseOperation, int count, int batchSize, BatchCommitStrategy batchCommitStrategy) {
        List<Agent> students = Collections.nCopies(count, TableReference.agents().getSample());
        final UnmodifiableIterator<List<Agent>> partition =
                Iterators.partition(students.iterator(), batchSize);
        final Instant begin = Instant.now();
        partition.forEachRemaining(aBatchOfStudents -> {
            someDatabaseOperation.insert(aBatchOfStudents);
            if (batchCommitStrategy == BatchCommitStrategy.PER_BATCH) {
                someDatabaseOperation.commit();
            }
        });
        if (batchCommitStrategy == BatchCommitStrategy.FINALLY) {
            someDatabaseOperation.commit();
        }
        final Instant end = Instant.now();
        final int actualCount = someDatabaseOperation.count();
        Assert.assertEquals(count, actualCount);
        testReporter.record("Temps nécessaire", begin, end);
    }

    enum BatchCommitStrategy {
        PER_BATCH, FINALLY
    }

    @Test
    @AppWhiteTest(displayName = "Pendant une minute, faire des insertions de 1000 lignes")
    @Ignore("Échoue et montre ainsi que MongoDB n'arrive pas à encaisser")
    public void insertOneMillionPerTenThousandsBatch() {
        final LocalDateTime end = LocalDateTime.now().plus(Duration.ofMinutes(1));
        int done = 0;
        while (LocalDateTime.now().isBefore(end)) {
            List<Agent> students = Collections.nCopies(1000, TableReference.agents().getSample());
            try {
                someDatabaseOperation.insert(students);
            } catch (MongoCommandException e) {
                final long secondBeforeEnd = Duration.between(LocalDateTime.now(), end).get(ChronoUnit.SECONDS);
                logger.debug("erreur Mongo", e);
                Assert.fail("incapable d'absorber plus de " + done * 1000 + " insertions, " + secondBeforeEnd + "s avant la fin du test");
            }
            done += 1;
        }
        someDatabaseOperation.commit();
        testReporter.record("Volumétrie (nombre de lots de 1000 lignes insérés)", done);
        logger.info("En insérant 1000 lignes pendant une minute, on y parvient " + done + " fois");
    }
}
