package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 * 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * http://ec.europa.eu/idabc/eupl5
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

import java.util.Collection;
import java.util.Date;

/**
 * Un Java bean pour représenter un document de la collection "conge"
 */
public class Conge {

    private String _id;
    private String _class;
    private String nir;
    private String numen;
    private String typeNsi;
    private String idSource;
    private String matricule;
    private String lotGestion;
    private int nombreConges;
    private Collection<InnerConge> conges;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public String getNir() {
        return nir;
    }

    public void setNir(String nir) {
        this.nir = nir;
    }

    public String getNumen() {
        return numen;
    }

    public void setNumen(String numen) {
        this.numen = numen;
    }

    public String getTypeNsi() {
        return typeNsi;
    }

    public void setTypeNsi(String typeNsi) {
        this.typeNsi = typeNsi;
    }

    public String getIdSource() {
        return idSource;
    }

    public void setIdSource(String idSource) {
        this.idSource = idSource;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getLotGestion() {
        return lotGestion;
    }

    public void setLotGestion(String lotGestion) {
        this.lotGestion = lotGestion;
    }

    public int getNombreConges() {
        return nombreConges;
    }

    public void setNombreConges(int nombreConges) {
        this.nombreConges = nombreConges;
    }

    public Collection<InnerConge> getConges() {
        return conges;
    }

    public void setConges(Collection<InnerConge> conges) {
        this.conges = conges;
    }

    public static class InnerConge {
        private String type;
        private Date dateDebut;
        private Date dateFinPrevi;
        private Date dateFin;
        private String temoinDebAMPM;
        private String temoinFinAMPM;
        private String temoinFinPrevAMPM;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Date getDateDebut() {
            return dateDebut;
        }

        public void setDateDebut(Date dateDebut) {
            this.dateDebut = dateDebut;
        }

        public Date getDateFinPrevi() {
            return dateFinPrevi;
        }

        public void setDateFinPrevi(Date dateFinPrevi) {
            this.dateFinPrevi = dateFinPrevi;
        }

        public Date getDateFin() {
            return dateFin;
        }

        public void setDateFin(Date dateFin) {
            this.dateFin = dateFin;
        }

        public String getTemoinDebAMPM() {
            return temoinDebAMPM;
        }

        public void setTemoinDebAMPM(String temoinDebAMPM) {
            this.temoinDebAMPM = temoinDebAMPM;
        }

        public String getTemoinFinAMPM() {
            return temoinFinAMPM;
        }

        public void setTemoinFinAMPM(String temoinFinAMPM) {
            this.temoinFinAMPM = temoinFinAMPM;
        }

        public String getTemoinFinPrevAMPM() {
            return temoinFinPrevAMPM;
        }

        public void setTemoinFinPrevAMPM(String temoinFinPrevAMPM) {
            this.temoinFinPrevAMPM = temoinFinPrevAMPM;
        }
    }
}
