package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 * 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * http://ec.europa.eu/idabc/eupl5
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

import java.util.Collection;
import java.util.Date;

/**
 * Un Java bean pour représenter un document de la collection "agent"
 */
public class Agent {

    private String _id;
    private String _class;
    private String nir;
    private Collection<Mail> mails;
    private String numen;
    private Date dateFin;
    private String typeNSI;
    private Collection<Diplome> diplomes;
    private String idSource;
    private Date dateDebut;
    private EtatCivil etatCivil;
    private Etatcivil etatcivil;
    private String matricule;
    private String nomsPrenoms;
    private Date dateCertifNIR;
    private RecrutementInitial recrutementInitial;
    private SituationFamiliale situationFamiliale;
    private CoordonneesBancaires coordonneesBancaires;
    private Collection<CoordonneesTelephonique> coordonneesTelephoniques;
    private Statut statut;
    private Position position;
    private Adresse adresse;
    private Collection<Carriere> carrieres;
    private Categorie categorie;
    private String lotGestion;
    private ModaliteService modaliteService;
    private Collection<EnfantPersCharge> enfantsPersCharge;
    private Collection<AffectationAdministrative> affectationsAdministratives;
    private Collection<AffectationsOperationnelle> affectationsOperationnelles;
    private Boe boe;
    private HandicapInvalidite handicapInvalidite;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public String getNir() {
        return nir;
    }

    public void setNir(String nir) {
        this.nir = nir;
    }

    public Collection<Mail> getMails() {
        return mails;
    }

    public void setMails(Collection<Mail> mails) {
        this.mails = mails;
    }

    public String getNumen() {
        return numen;
    }

    public void setNumen(String numen) {
        this.numen = numen;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getTypeNSI() {
        return typeNSI;
    }

    public void setTypeNSI(String typeNSI) {
        this.typeNSI = typeNSI;
    }

    public Collection<Diplome> getDiplomes() {
        return diplomes;
    }

    public void setDiplomes(Collection<Diplome> diplomes) {
        this.diplomes = diplomes;
    }

    public String getIdSource() {
        return idSource;
    }

    public void setIdSource(String idSource) {
        this.idSource = idSource;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public EtatCivil getEtatCivil() {
        return etatCivil;
    }

    public void setEtatCivil(EtatCivil etatCivil) {
        this.etatCivil = etatCivil;
    }

    public Etatcivil getEtatcivil() {
        return etatcivil;
    }

    public void setEtatcivil(Etatcivil etatcivil) {
        this.etatcivil = etatcivil;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getNomsPrenoms() {
        return nomsPrenoms;
    }

    public void setNomsPrenoms(String nomsPrenoms) {
        this.nomsPrenoms = nomsPrenoms;
    }

    public Date getDateCertifNIR() {
        return dateCertifNIR;
    }

    public void setDateCertifNIR(Date dateCertifNIR) {
        this.dateCertifNIR = dateCertifNIR;
    }

    public RecrutementInitial getRecrutementInitial() {
        return recrutementInitial;
    }

    public void setRecrutementInitial(RecrutementInitial recrutementInitial) {
        this.recrutementInitial = recrutementInitial;
    }

    public SituationFamiliale getSituationFamiliale() {
        return situationFamiliale;
    }

    public void setSituationFamiliale(SituationFamiliale situationFamiliale) {
        this.situationFamiliale = situationFamiliale;
    }

    public CoordonneesBancaires getCoordonneesBancaires() {
        return coordonneesBancaires;
    }

    public void setCoordonneesBancaires(CoordonneesBancaires coordonneesBancaires) {
        this.coordonneesBancaires = coordonneesBancaires;
    }

    public Collection<CoordonneesTelephonique> getCoordonneesTelephoniques() {
        return coordonneesTelephoniques;
    }

    public void setCoordonneesTelephoniques(Collection<CoordonneesTelephonique> coordonneesTelephoniques) {
        this.coordonneesTelephoniques = coordonneesTelephoniques;
    }

    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public Collection<Carriere> getCarrieres() {
        return carrieres;
    }

    public void setCarrieres(Collection<Carriere> carrieres) {
        this.carrieres = carrieres;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public String getLotGestion() {
        return lotGestion;
    }

    public void setLotGestion(String lotGestion) {
        this.lotGestion = lotGestion;
    }

    public ModaliteService getModaliteService() {
        return modaliteService;
    }

    public void setModaliteService(ModaliteService modaliteService) {
        this.modaliteService = modaliteService;
    }

    public Collection<EnfantPersCharge> getEnfantsPersCharge() {
        return enfantsPersCharge;
    }

    public void setEnfantsPersCharge(Collection<EnfantPersCharge> enfantsPersCharge) {
        this.enfantsPersCharge = enfantsPersCharge;
    }

    public Collection<AffectationAdministrative> getAffectationsAdministratives() {
        return affectationsAdministratives;
    }

    public void setAffectationsAdministratives(Collection<AffectationAdministrative> affectationsAdministratives) {
        this.affectationsAdministratives = affectationsAdministratives;
    }

    public Collection<AffectationsOperationnelle> getAffectationsOperationnelles() {
        return affectationsOperationnelles;
    }

    public void setAffectationsOperationnelles(Collection<AffectationsOperationnelle> affectationsOperationnelles) {
        this.affectationsOperationnelles = affectationsOperationnelles;
    }

    public Boe getBoe() {
        return boe;
    }

    public void setBoe(Boe boe) {
        this.boe = boe;
    }

    public HandicapInvalidite getHandicapInvalidite() {
        return handicapInvalidite;
    }

    public void setHandicapInvalidite(HandicapInvalidite handicapInvalidite) {
        this.handicapInvalidite = handicapInvalidite;
    }

    public static class Mail {
        private String type;
        private String libelle;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }
    }

    public static class CoordonneesTelephonique {
        private String type;
        private String libelle;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }
    }

    public static class Diplome {
        private String type;
        private String paysDiplome;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getPaysDiplome() {
            return paysDiplome;
        }

        public void setPaysDiplome(String paysDiplome) {
            this.paysDiplome = paysDiplome;
        }
    }

    public static class EtatCivil {
        private String nom;
        private String sexe;
        private String prenom;
        private String civilite;
        private String nomUsage;
        private Date dateNaissance;
        private String deptNaissance;
        private String paysNaissance;
        private String villeNaissance;
        private Date dateDeces;

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public String getSexe() {
            return sexe;
        }

        public void setSexe(String sexe) {
            this.sexe = sexe;
        }

        public String getPrenom() {
            return prenom;
        }

        public void setPrenom(String prenom) {
            this.prenom = prenom;
        }

        public String getCivilite() {
            return civilite;
        }

        public void setCivilite(String civilite) {
            this.civilite = civilite;
        }

        public String getNomUsage() {
            return nomUsage;
        }

        public void setNomUsage(String nomUsage) {
            this.nomUsage = nomUsage;
        }

        public Date getDateNaissance() {
            return dateNaissance;
        }

        public void setDateNaissance(Date dateNaissance) {
            this.dateNaissance = dateNaissance;
        }

        public String getDeptNaissance() {
            return deptNaissance;
        }

        public void setDeptNaissance(String deptNaissance) {
            this.deptNaissance = deptNaissance;
        }

        public String getPaysNaissance() {
            return paysNaissance;
        }

        public void setPaysNaissance(String paysNaissance) {
            this.paysNaissance = paysNaissance;
        }

        public String getVilleNaissance() {
            return villeNaissance;
        }

        public void setVilleNaissance(String villeNaissance) {
            this.villeNaissance = villeNaissance;
        }

        public Date getDateDeces() {
            return dateDeces;
        }

        public void setDateDeces(Date dateDeces) {
            this.dateDeces = dateDeces;
        }
    }

    public static class Etatcivil {
        private String nomusage;

        public String getNomusage() {
            return nomusage;
        }

        public void setNomusage(String nomusage) {
            this.nomusage = nomusage;
        }
    }

    public static class RecrutementInitial {
        private Date dateEntreeFP;
        private String modeEntreeFP;
        private Date dateEntreeFPE;

        public Date getDateEntreeFP() {
            return dateEntreeFP;
        }

        public void setDateEntreeFP(Date dateEntreeFP) {
            this.dateEntreeFP = dateEntreeFP;
        }

        public String getModeEntreeFP() {
            return modeEntreeFP;
        }

        public void setModeEntreeFP(String modeEntreeFP) {
            this.modeEntreeFP = modeEntreeFP;
        }

        public Date getDateEntreeFPE() {
            return dateEntreeFPE;
        }

        public void setDateEntreeFPE(Date dateEntreeFPE) {
            this.dateEntreeFPE = dateEntreeFPE;
        }
    }

    public static class SituationFamiliale {
        private String type;
        private Date dateFin;
        private Date dateDebut;
        private String indicateurParentIsole;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Date getDateFin() {
            return dateFin;
        }

        public void setDateFin(Date dateFin) {
            this.dateFin = dateFin;
        }

        public Date getDateDebut() {
            return dateDebut;
        }

        public void setDateDebut(Date dateDebut) {
            this.dateDebut = dateDebut;
        }

        public String getIndicateurParentIsole() {
            return indicateurParentIsole;
        }

        public void setIndicateurParentIsole(String indicateurParentIsole) {
            this.indicateurParentIsole = indicateurParentIsole;
        }
    }

    public static class CoordonneesBancaires {
        private String bic;
        private String iban;
        private String modeReglement;

        public String getBic() {
            return bic;
        }

        public void setBic(String bic) {
            this.bic = bic;
        }

        public String getIban() {
            return iban;
        }

        public void setIban(String iban) {
            this.iban = iban;
        }

        public String getModeReglement() {
            return modeReglement;
        }

        public void setModeReglement(String modeReglement) {
            this.modeReglement = modeReglement;
        }
    }

    public static class Statut {
        private String code;
        private Date dateEntree;
        private Date datePrevueFinStage;
        private Date dateTitularisation;
        private String dureeProlongationStage;
        private String typeSituationStage;
        private String commentaireProlStage;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Date getDateEntree() {
            return dateEntree;
        }

        public void setDateEntree(Date dateEntree) {
            this.dateEntree = dateEntree;
        }

        public Date getDatePrevueFinStage() {
            return datePrevueFinStage;
        }

        public void setDatePrevueFinStage(Date datePrevueFinStage) {
            this.datePrevueFinStage = datePrevueFinStage;
        }

        public Date getDateTitularisation() {
            return dateTitularisation;
        }

        public void setDateTitularisation(Date dateTitularisation) {
            this.dateTitularisation = dateTitularisation;
        }

        public String getDureeProlongationStage() {
            return dureeProlongationStage;
        }

        public void setDureeProlongationStage(String dureeProlongationStage) {
            this.dureeProlongationStage = dureeProlongationStage;
        }

        public String getTypeSituationStage() {
            return typeSituationStage;
        }

        public void setTypeSituationStage(String typeSituationStage) {
            this.typeSituationStage = typeSituationStage;
        }

        public String getCommentaireProlStage() {
            return commentaireProlStage;
        }

        public void setCommentaireProlStage(String commentaireProlStage) {
            this.commentaireProlStage = commentaireProlStage;
        }
    }

    public static class Position {
        private String code;
        private Date dateDebut;
        private Date dateFinPrevi;
        private Date dateFin;
        private String indicateurProlong;
        private String motifCessFonc;
        private String identifiantEnfPersCharge;
        private String motifProlongActivite;

        public String getMotifProlongActivite() {
            return motifProlongActivite;
        }

        public void setMotifProlongActivite(String motifProlongActivite) {
            this.motifProlongActivite = motifProlongActivite;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Date getDateDebut() {
            return dateDebut;
        }

        public void setDateDebut(Date dateDebut) {
            this.dateDebut = dateDebut;
        }

        public Date getDateFinPrevi() {
            return dateFinPrevi;
        }

        public void setDateFinPrevi(Date dateFinPrevi) {
            this.dateFinPrevi = dateFinPrevi;
        }

        public Date getDateFin() {
            return dateFin;
        }

        public void setDateFin(Date dateFin) {
            this.dateFin = dateFin;
        }

        public String getIndicateurProlong() {
            return indicateurProlong;
        }

        public void setIndicateurProlong(String indicateurProlong) {
            this.indicateurProlong = indicateurProlong;
        }

        public String getMotifCessFonc() {
            return motifCessFonc;
        }

        public void setMotifCessFonc(String motifCessFonc) {
            this.motifCessFonc = motifCessFonc;
        }

        public String getIdentifiantEnfPersCharge() {
            return identifiantEnfPersCharge;
        }

        public void setIdentifiantEnfPersCharge(String identifiantEnfPersCharge) {
            this.identifiantEnfPersCharge = identifiantEnfPersCharge;
        }
    }

    public static class Adresse {
        private String type;
        private String nature;
        private String numero;
        private String extension;
        private String localite;
        private String codeInsee;
        private String codePostal;
        private String libelleVoie;
        private String paysImplantation;
        private String complement;
        private String lieuDit;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getNature() {
            return nature;
        }

        public void setNature(String nature) {
            this.nature = nature;
        }

        public String getNumero() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero = numero;
        }

        public String getExtension() {
            return extension;
        }

        public void setExtension(String extension) {
            this.extension = extension;
        }

        public String getLocalite() {
            return localite;
        }

        public void setLocalite(String localite) {
            this.localite = localite;
        }

        public String getCodeInsee() {
            return codeInsee;
        }

        public void setCodeInsee(String codeInsee) {
            this.codeInsee = codeInsee;
        }

        public String getCodePostal() {
            return codePostal;
        }

        public void setCodePostal(String codePostal) {
            this.codePostal = codePostal;
        }

        public String getLibelleVoie() {
            return libelleVoie;
        }

        public void setLibelleVoie(String libelleVoie) {
            this.libelleVoie = libelleVoie;
        }

        public String getPaysImplantation() {
            return paysImplantation;
        }

        public void setPaysImplantation(String paysImplantation) {
            this.paysImplantation = paysImplantation;
        }

        public String getComplement() {
            return complement;
        }

        public void setComplement(String complement) {
            this.complement = complement;
        }

        public String getLieuDit() {
            return lieuDit;
        }

        public void setLieuDit(String lieuDit) {
            this.lieuDit = lieuDit;
        }
    }

    public static class Carriere {
        private Corps corps;
        private Grade grade;
        private Echelon echelon;
        private Chevron chevron;
        private String indicateur;
        private String typologiePop;

        public Corps getCorps() {
            return corps;
        }

        public void setCorps(Corps corps) {
            this.corps = corps;
        }

        public Grade getGrade() {
            return grade;
        }

        public void setGrade(Grade grade) {
            this.grade = grade;
        }

        public Chevron getChevron() {
            return chevron;
        }

        public void setChevron(Chevron chevron) {
            this.chevron = chevron;
        }

        public Echelon getEchelon() {
            return echelon;
        }

        public void setEchelon(Echelon echelon) {
            this.echelon = echelon;
        }

        public String getIndicateur() {
            return indicateur;
        }

        public void setIndicateur(String indicateur) {
            this.indicateur = indicateur;
        }

        public String getTypologiePop() {
            return typologiePop;
        }

        public void setTypologiePop(String typologiePop) {
            this.typologiePop = typologiePop;
        }

        public static class Corps {
            private String code;
            private String typePers;
            private String modeAcces;
            private Date dateEntree;
            private String modeSortie;
            private Date dateIntegration;
            private Date dateConcours;

            public Date getDateConcours() {
                return dateConcours;
            }

            public void setDateConcours(Date dateConcours) {
                this.dateConcours = dateConcours;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getTypePers() {
                return typePers;
            }

            public void setTypePers(String typePers) {
                this.typePers = typePers;
            }

            public String getModeAcces() {
                return modeAcces;
            }

            public void setModeAcces(String modeAcces) {
                this.modeAcces = modeAcces;
            }

            public Date getDateEntree() {
                return dateEntree;
            }

            public void setDateEntree(Date dateEntree) {
                this.dateEntree = dateEntree;
            }

            public String getModeSortie() {
                return modeSortie;
            }

            public void setModeSortie(String modeSortie) {
                this.modeSortie = modeSortie;
            }

            public Date getDateIntegration() {
                return dateIntegration;
            }

            public void setDateIntegration(Date dateIntegration) {
                this.dateIntegration = dateIntegration;
            }
        }

        public static class Grade {
            private String code;
            private String modeAcces;
            private Date dateEntree;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getModeAcces() {
                return modeAcces;
            }

            public void setModeAcces(String modeAcces) {
                this.modeAcces = modeAcces;
            }

            public Date getDateEntree() {
                return dateEntree;
            }

            public void setDateEntree(Date dateEntree) {
                this.dateEntree = dateEntree;
            }
        }

        public static class Echelon {
            private String code;
            private String grille;
            private String modeAcces;
            private Date dateEntree;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getGrille() {
                return grille;
            }

            public void setGrille(String grille) {
                this.grille = grille;
            }

            public String getModeAcces() {
                return modeAcces;
            }

            public void setModeAcces(String modeAcces) {
                this.modeAcces = modeAcces;
            }

            public Date getDateEntree() {
                return dateEntree;
            }

            public void setDateEntree(Date dateEntree) {
                this.dateEntree = dateEntree;
            }
        }

        public static class Chevron {
            private String code;
            private Date dateEffet;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public Date getDateEffet() {
                return dateEffet;
            }

            public void setDateEffet(Date dateEffet) {
                this.dateEffet = dateEffet;
            }
        }
    }

    public static class Categorie {
        private String code;
        private Date dateEntree;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Date getDateEntree() {
            return dateEntree;
        }

        public void setDateEntree(Date dateEntree) {
            this.dateEntree = dateEntree;
        }
    }

    public static class ModaliteService {
        private String code;
        private Date dateFin;
        private Date dateDebut;
        private int numerateur;
        private Date dateFinPrevi;
        private int denominateur;
        private boolean indicateurSusp;
        private int quotiteService;
        private String modaliteExercice;
        private String tempsTravailHebdo;
        private boolean temoinSurcotisation;
        private int quotiteTempsIncomplet;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Date getDateFin() {
            return dateFin;
        }

        public void setDateFin(Date dateFin) {
            this.dateFin = dateFin;
        }

        public Date getDateDebut() {
            return dateDebut;
        }

        public void setDateDebut(Date dateDebut) {
            this.dateDebut = dateDebut;
        }

        public int getNumerateur() {
            return numerateur;
        }

        public void setNumerateur(int numerateur) {
            this.numerateur = numerateur;
        }

        public Date getDateFinPrevi() {
            return dateFinPrevi;
        }

        public void setDateFinPrevi(Date dateFinPrevi) {
            this.dateFinPrevi = dateFinPrevi;
        }

        public int getDenominateur() {
            return denominateur;
        }

        public void setDenominateur(int denominateur) {
            this.denominateur = denominateur;
        }

        public boolean isIndicateurSusp() {
            return indicateurSusp;
        }

        public void setIndicateurSusp(boolean indicateurSusp) {
            this.indicateurSusp = indicateurSusp;
        }

        public int getQuotiteService() {
            return quotiteService;
        }

        public void setQuotiteService(int quotiteService) {
            this.quotiteService = quotiteService;
        }

        public String getModaliteExercice() {
            return modaliteExercice;
        }

        public void setModaliteExercice(String modaliteExercice) {
            this.modaliteExercice = modaliteExercice;
        }

        public String getTempsTravailHebdo() {
            return tempsTravailHebdo;
        }

        public void setTempsTravailHebdo(String tempsTravailHebdo) {
            this.tempsTravailHebdo = tempsTravailHebdo;
        }

        public boolean isTemoinSurcotisation() {
            return temoinSurcotisation;
        }

        public void setTemoinSurcotisation(boolean temoinSurcotisation) {
            this.temoinSurcotisation = temoinSurcotisation;
        }

        public int getQuotiteTempsIncomplet() {
            return quotiteTempsIncomplet;
        }

        public void setQuotiteTempsIncomplet(int quotiteTempsIncomplet) {
            this.quotiteTempsIncomplet = quotiteTempsIncomplet;
        }
    }

    public static class EnfantPersCharge {
        private String id;
        private String nom;
        private String sexe;
        private String prenom;
        private String indicateur;
        private Date dateNaissance;
        private Date dateDeces;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public String getSexe() {
            return sexe;
        }

        public void setSexe(String sexe) {
            this.sexe = sexe;
        }

        public String getPrenom() {
            return prenom;
        }

        public void setPrenom(String prenom) {
            this.prenom = prenom;
        }

        public String getIndicateur() {
            return indicateur;
        }

        public void setIndicateur(String indicateur) {
            this.indicateur = indicateur;
        }

        public Date getDateNaissance() {
            return dateNaissance;
        }

        public void setDateNaissance(Date dateNaissance) {
            this.dateNaissance = dateNaissance;
        }

        public Date getDateDeces() {
            return dateDeces;
        }

        public void setDateDeces(Date dateDeces) {
            this.dateDeces = dateDeces;
        }
    }

    public static class AffectationAdministrative {
        private String cause;
        private Date dateFin;
        private String academie;
        private String modalite;
        private Date dateDebut;
        private String identifiantUS;
        private String libelleEtablissement;
        private boolean indicateurAffectGestion;
        private Date dateFinPrevi;
        private Date dateInstallation;
        private String derniereAcademie;
        private Date dateFinDerniereAcademie;

        public Date getDateFinDerniereAcademie() {
            return dateFinDerniereAcademie;
        }

        public void setDateFinDerniereAcademie(Date dateFinDerniereAcademie) {
            this.dateFinDerniereAcademie = dateFinDerniereAcademie;
        }

        public String getCause() {
            return cause;
        }

        public void setCause(String cause) {
            this.cause = cause;
        }

        public Date getDateFin() {
            return dateFin;
        }

        public void setDateFin(Date dateFin) {
            this.dateFin = dateFin;
        }

        public String getAcademie() {
            return academie;
        }

        public void setAcademie(String academie) {
            this.academie = academie;
        }

        public String getModalite() {
            return modalite;
        }

        public void setModalite(String modalite) {
            this.modalite = modalite;
        }

        public Date getDateDebut() {
            return dateDebut;
        }

        public void setDateDebut(Date dateDebut) {
            this.dateDebut = dateDebut;
        }

        public String getIdentifiantUS() {
            return identifiantUS;
        }

        public void setIdentifiantUS(String identifiantUS) {
            this.identifiantUS = identifiantUS;
        }

        public boolean isIndicateurAffectGestion() {
            return indicateurAffectGestion;
        }

        public void setIndicateurAffectGestion(boolean indicateurAffectGestion) {
            this.indicateurAffectGestion = indicateurAffectGestion;
        }

        public String getLibelleEtablissement() {
            return libelleEtablissement;
        }

        public void setLibelleEtablissement(String libelleEtablissement) {
            this.libelleEtablissement = libelleEtablissement;
        }

        public Date getDateFinPrevi() {
            return dateFinPrevi;
        }

        public void setDateFinPrevi(Date dateFinPrevi) {
            this.dateFinPrevi = dateFinPrevi;
        }

        public Date getDateInstallation() {
            return dateInstallation;
        }

        public void setDateInstallation(Date dateInstallation) {
            this.dateInstallation = dateInstallation;
        }

        public String getDerniereAcademie() {
            return derniereAcademie;
        }

        public void setDerniereAcademie(String derniereAcademie) {
            this.derniereAcademie = derniereAcademie;
        }
    }

    public static class AffectationsOperationnelle {
        private String nature;
        private Date dateFin;
        private Date dateDebut;
        private String identifiantUS;
        private boolean affectRecrutLoc;
        private String identifiantPoste;
        private boolean affectSupprEmploi;
        private int pourcentAffectPoste;
        private String identifiantActAccessoire;
        private boolean indicateurAffectPrincipale;

        public String getNature() {
            return nature;
        }

        public void setNature(String nature) {
            this.nature = nature;
        }

        public Date getDateFin() {
            return dateFin;
        }

        public void setDateFin(Date dateFin) {
            this.dateFin = dateFin;
        }

        public Date getDateDebut() {
            return dateDebut;
        }

        public void setDateDebut(Date dateDebut) {
            this.dateDebut = dateDebut;
        }

        public String getIdentifiantUS() {
            return identifiantUS;
        }

        public void setIdentifiantUS(String identifiantUS) {
            this.identifiantUS = identifiantUS;
        }

        public boolean isAffectRecrutLoc() {
            return affectRecrutLoc;
        }

        public void setAffectRecrutLoc(boolean affectRecrutLoc) {
            this.affectRecrutLoc = affectRecrutLoc;
        }

        public String getIdentifiantPoste() {
            return identifiantPoste;
        }

        public void setIdentifiantPoste(String identifiantPoste) {
            this.identifiantPoste = identifiantPoste;
        }

        public boolean isAffectSupprEmploi() {
            return affectSupprEmploi;
        }

        public void setAffectSupprEmploi(boolean affectSupprEmploi) {
            this.affectSupprEmploi = affectSupprEmploi;
        }

        public int getPourcentAffectPoste() {
            return pourcentAffectPoste;
        }

        public void setPourcentAffectPoste(int pourcentAffectPoste) {
            this.pourcentAffectPoste = pourcentAffectPoste;
        }

        public String getIdentifiantActAccessoire() {
            return identifiantActAccessoire;
        }

        public void setIdentifiantActAccessoire(String identifiantActAccessoire) {
            this.identifiantActAccessoire = identifiantActAccessoire;
        }

        public boolean isIndicateurAffectPrincipale() {
            return indicateurAffectPrincipale;
        }

        public void setIndicateurAffectPrincipale(boolean indicateurAffectPrincipale) {
            this.indicateurAffectPrincipale = indicateurAffectPrincipale;
        }
    }

    public static class Boe {
        private Date dateDebut;
        private Date dateFin;
        private String categorie;

        public Date getDateDebut() {
            return dateDebut;
        }

        public void setDateDebut(Date dateDebut) {
            this.dateDebut = dateDebut;
        }

        public Date getDateFin() {
            return dateFin;
        }

        public void setDateFin(Date dateFin) {
            this.dateFin = dateFin;
        }

        public String getCategorie() {
            return categorie;
        }

        public void setCategorie(String categorie) {
            this.categorie = categorie;
        }
    }

    public static class HandicapInvalidite {
        private boolean recoHandicap;
        private String tauxInvalidite;

        public boolean isRecoHandicap() {
            return recoHandicap;
        }

        public void setRecoHandicap(boolean recoHandicap) {
            this.recoHandicap = recoHandicap;
        }

        public String getTauxInvalidite() {
            return tauxInvalidite;
        }

        public void setTauxInvalidite(String tauxInvalidite) {
            this.tauxInvalidite = tauxInvalidite;
        }
    }
}
