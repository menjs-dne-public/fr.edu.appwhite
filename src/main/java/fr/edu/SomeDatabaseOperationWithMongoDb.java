package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 * 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * http://ec.europa.eu/idabc/eupl5
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.ClientSession;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Indexes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Implémentation de {@link SomeDatabaseOperation} avec MongoDB.
 *
 * L'implémentation est partielle et les sous-classes permettent de faire les opérations selon
 * qu'on soit en mode transactionnel ou non.
 */
public abstract class SomeDatabaseOperationWithMongoDb implements SomeDatabaseOperation {

    private static final AutoCloseable NO_DATABASE_TO_CLOSE = () -> {};

    private static final Logger logger = LogManager.getLogger(SomeDatabaseOperationWithMongoDb.class);

    private static final CodecRegistry POJO_CODEC_REGISTRY = CodecRegistries.fromRegistries(
            MongoClientSettings.getDefaultCodecRegistry(),
            CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build())
    );

    private static final AppWhiteJson APP_WHITE_JSON = new AppWhiteJson();

    private final AutoCloseable mongoToClose;

    private final MongoClient mongoClient;

    private SomeDatabaseOperationWithMongoDb(AutoCloseable mongoToClose, MongoClient mongoClient) {
        this.mongoToClose = mongoToClose;
        this.mongoClient = mongoClient;
    }

    public static SomeDatabaseOperationWithMongoDb fromCredentials(String host, String port, String user, String password, boolean transactionSupported) {
        return fromCredentials(host, port, user, password, transactionSupported, NO_DATABASE_TO_CLOSE);
    }

    public static SomeDatabaseOperationWithMongoDb fromCredentials(String host, String port, String user, String password, boolean transactionSupported, AutoCloseable mongoToClose) {
        final String url = String.format("mongodb://%s:%s@%s:%s/?authSource=admin&retryWrites=false", user, password, host, port);
        return fromUrl(url, mongoToClose, transactionSupported);
    }

    public static SomeDatabaseOperationWithMongoDb fromCredentials(String host, String port, boolean transactionSupported) {
        final String url = String.format("mongodb://%s:%s/?authSource=admin&retryWrites=false", host, port);
        return fromUrl(url, NO_DATABASE_TO_CLOSE, transactionSupported);
    }

    public static SomeDatabaseOperationWithMongoDb fromUrl(String url, AutoCloseable mongoToClose, boolean transactionSupported) {
        logger.info("va créer un client MongoDB avec l'URL de connexion " + url);
        MongoClient mongoClient = MongoClients.create(url);
        if (transactionSupported) {
            ClientSession clientSession = mongoClient.startSession();
            clientSession.startTransaction();
            return new WithTransactionSupport(mongoToClose, mongoClient, clientSession);
        } else {
            return new WithoutTransactionSupport(mongoToClose, mongoClient);
        }
    }

    public int doSomething() {
        final MongoDatabase mongoDatabase = mongoClient.getDatabase("admin");
        final MongoCollection<Document> mongoCollection = mongoDatabase.getCollection("system.version");
        Document versionDocument = mongoCollection.find(Filters.eq("_id", "featureCompatibilityVersion")).first();
        logger.info("Connexion réussie à MongoDB version " +  versionDocument.get("version"));
        return 1;
    }

    @Override
    public Agent findAgentById(String agentId) {
        final MongoCollection<Agent> collection = getAgentCollection();
        final Bson bson = Filters.eq("_id", new ObjectId(agentId));
        Agent agent = doFind(collection, bson).first();
        return agent;
    }

    @Override
    public List<Agent> findAgentsByIds(Set<String> agentIds) {
        final Set<ObjectId> objectIds = agentIds.stream()
                .map(ObjectId::new)
                .collect(Collectors.toUnmodifiableSet());
        final MongoCollection<Agent> collection = getAgentCollection();
        final Bson bson = Filters.in("_id", objectIds);
        final List<Agent> agents = new LinkedList<>();
        Iterables.addAll(agents, doFind(collection, bson));
        return agents;
    }

    protected abstract FindIterable<Agent> doFind(MongoCollection<Agent> collection, Bson bson);

    @Override
    public void insert(List<Agent> students) {
        final MongoCollection<Agent> collection = getAgentCollection();
        doInsert(collection, students);
    }

    protected abstract void doInsert(MongoCollection<Agent> collection, List<Agent> students);

    private MongoCollection<Agent> getAgentCollection() {
        return getMongoCollection(TableReference.agents());
    }

    private MongoCollection<Conge> getCongeCollection() {
        return getMongoCollection(TableReference.conges());
    }

    private <E> MongoCollection<E> getMongoCollection(TableReference<E> tableReference) {
        final MongoDatabase mongoDatabase = mongoClient.getDatabase("appwhite").withCodecRegistry(POJO_CODEC_REGISTRY);
        final MongoCollection<E> collection = mongoDatabase.getCollection(tableReference.getTableName(), tableReference.getDocumentType());
        return collection;
    }

    @Override
    public int count() {
        final MongoCollection<Agent> collection = getAgentCollection();
        return (int) doCount(collection);
    }

    protected abstract long doCount(MongoCollection<Agent> collection);

    @Override
    public void close() {
        logger.info("fermeture de la connexion à MongoDB " + mongoClient);
        mongoClient.close();
        try {
            logger.info("fermeture de MongoDB " + mongoToClose);
            mongoToClose.close();
        } catch (Exception e) {
            throw new RuntimeException("impossible de fermer MongoDB " + mongoToClose, e);
        }
    }

    @Override
    public void forEachAgent(Consumer<? super Agent> consumer) {
        final MongoCollection<Agent> collection = getAgentCollection();
        final FindIterable<Agent> agents = doFind(collection).batchSize(50);
        final MongoCursor<Agent> iterator = agents.iterator();
        iterator.forEachRemaining(consumer);
    }

    @Override
    public List<Agent> searchAgents(String dateFinIso) {
        final MongoCollection<Agent> collection = getAgentCollection();
        final String json = ""
                + "{"
                + "  \"dateFin\":{"
                + "    \"$gte\":{"
                + "      \"$date\":\"" + dateFinIso + "\""
                + "    }"
                + "  }"
                + "}"
                ;
        final BsonDocument bson = BsonDocument.parse(json);
        final List<Agent> agents = new LinkedList<>();
        Iterables.addAll(agents, doFind(collection, bson));
        return agents;
    }

    @Override
    public List<Agent> searchAgents(String dateIso, Set<String> nirs, Set<String> carriereGradeCodes) {
        final MongoCollection<Agent> collection = getAgentCollection();
        String json = ""
                + "{"
                + "  \"dateFin\":{"
                + "    \"$gte\":{"
                + "      \"$date\":\"" + dateIso + "\""
                + "    }"
                + "  },"
                + "  \"dateDebut\":{"
                + "    \"$lte\":{"
                + "      \"$date\":\"" + dateIso + "\""
                + "    }"
                + "  },"
                + "  \"carrieres.grade.code\":{"
                + "    \"$in\":" + APP_WHITE_JSON.toJsonString(carriereGradeCodes)
                + "  },"
                + "  \"nir\":{"
                + "    \"$in\":" + APP_WHITE_JSON.toJsonString(nirs)
                + "  }"
                + "}";
        final BsonDocument bson = BsonDocument.parse(json);
        final List<Agent> agents = new LinkedList<>();
        Iterables.addAll(agents, doFind(collection, bson));
        return agents;
    }

    @Override
    public List<Agent> searchAgents(Set<String> carriereGradeCodes) {
        final MongoCollection<Agent> collection = getAgentCollection();
        String json = ""
                + "{"
                + "  \"carrieres.grade.code\":{"
                + "    \"$in\":" + APP_WHITE_JSON.toJsonString(carriereGradeCodes)
                + "  }"
                + "}";
        final BsonDocument bson = BsonDocument.parse(json);
        final List<Agent> agents = new LinkedList<>();
        Iterables.addAll(agents, doFind(collection, bson));
        return agents;
    }

    @Override
    public void indexAgentsDateFin() {
        final MongoCollection<Agent> collection = getAgentCollection();
        final Bson index = Indexes.ascending("dateFin");
        doCreateIndex(collection, index);
    }

    @Override
    public void indexAgentNirsUsingHash() {
        final MongoCollection<Agent> collection = getAgentCollection();
        final Bson index = Indexes.hashed("nir");
        doCreateIndex(collection, index);
    }

    @Override
    public List<Agent> findAgentByNir(String nir) {
        final MongoCollection<Agent> collection = getAgentCollection();
        final Bson filter = Filters.eq("nir", nir);
        final List<Agent> agents = new LinkedList<>();
        Iterables.addAll(agents, doFind(collection, filter));
        return agents;
    }

    @Override
    public void indexAgentsDateFinDateDebutNir() {
        final MongoCollection<Agent> collection = getAgentCollection();
        final Bson index = Indexes.compoundIndex(
                Indexes.ascending("dateFin"),
                Indexes.ascending("dateDebut"),
                Indexes.hashed("nir")
        );
        doCreateIndex(collection, index);
    }

    @Override
    public void indexAgentsCarriereGradeCodes() {
        // on ne sait pas comment faire
    }

    @Override
    public int sumNombreConges() {
        final MongoCollection<Conge> collection = getCongeCollection();
        String json = ""
                + "{"
                + "  \"$group\":{"
                + "    \"_id\": \"whatever\","
                + "    \"sumNombreConges\":{"
                + "      \"$sum\": \"$nombreConges\""
                + "    }"
                + "  }"
                + "}";
        final BsonDocument bson = BsonDocument.parse(json);
        final AggregateIterable<BsonDocument> aggregate = doAggregate(collection, bson);
        return aggregate.iterator().next().get("sumNombreConges").asInt32().intValue();
    }

    @Override
    public Map<String, Integer> countAgentPerDepartementNaissance() {
        final MongoCollection<Agent> collection = getAgentCollection();
        String json = ""
                + "{"
                + "  \"$group\":{"
                + "    \"_id\": \"$etatCivil.deptNaissance\","
                + "    \"count\":{"
                + "      \"$count\":{}"
                + "    }"
                + "  }"
                + "}";
        final BsonDocument bson = BsonDocument.parse(json);
        final AggregateIterable<BsonDocument> aggregate = doAggregate(collection, bson);
        final Map<String, Integer> countAgentPerDepartementNaissance = new HashMap<>();
        for (BsonDocument bsonDocument : aggregate) {
            final int count = bsonDocument.getInt32("count").intValue();
            if (bsonDocument.isNull("_id")) {
                countAgentPerDepartementNaissance.put(null, count);
            } else {
                countAgentPerDepartementNaissance.put(bsonDocument.getString("_id").getValue(), count);
            }
        }
        return countAgentPerDepartementNaissance;
    }

    protected abstract <TResult> AggregateIterable<BsonDocument> doAggregate(MongoCollection<TResult> collection, BsonDocument bson);

    protected abstract String doCreateIndex(MongoCollection<Agent> collection, Bson index);

    protected abstract FindIterable<Agent> doFind(MongoCollection<Agent> collection);

    /**
     * Ici, on réalise chaque opération dans une transaction.
     */
    private static class WithTransactionSupport extends SomeDatabaseOperationWithMongoDb {

        private final ClientSession clientSession;

        private WithTransactionSupport(AutoCloseable mongoToClose, MongoClient mongoClient, ClientSession clientSession) {
            super(mongoToClose, mongoClient);
            this.clientSession = clientSession;
        }

        @Override
        public void commit() {
            clientSession.commitTransaction();
            clientSession.startTransaction();
        }

        @Override
        public void close() {
            logger.info("fermeture de la session à MongoDB " + clientSession);
            clientSession.close();
            super.close();
        }

        @Override
        protected long doCount(MongoCollection<Agent> collection) {
            return collection.countDocuments(clientSession);
        }

        @Override
        protected void doInsert(MongoCollection<Agent> collection, List<Agent> students) {
            collection.insertMany(clientSession, students);
        }

        @Override
        protected FindIterable<Agent> doFind(MongoCollection<Agent> collection, Bson bson) {
            return collection.find(clientSession, bson);
        }

        @Override
        protected FindIterable<Agent> doFind(MongoCollection<Agent> collection) {
            return collection.find(clientSession);
        }

        @Override
        protected String doCreateIndex(MongoCollection<Agent> collection, Bson index) {
            {
                // sinon, on tombe sur com.mongodb.MongoCommandException: Command failed with error 263 (OperationNotSupportedInTransaction): 'Cannot create new indexes on existing collection appwhite.agent in a multi-document transaction.' on server localhost:49379. The full response is {"ok": 0.0, "errmsg": "Cannot create new indexes on existing collection appwhite.agent in a multi-document transaction.", "code": 263, "codeName": "OperationNotSupportedInTransaction", "$clusterTime": {"clusterTime": {"$timestamp": {"t": 1635953646, "i": 1}}, "signature": {"hash": {"$binary": {"base64": "AAAAAAAAAAAAAAAAAAAAAAAAAAA=", "subType": "00"}}, "keyId": 0}}, "operationTime": {"$timestamp": {"t": 1635953646, "i": 1}}}
                clientSession.commitTransaction();
            }
            return collection.createIndex(clientSession, index);
        }

        @Override
        protected <TResult> AggregateIterable<BsonDocument>  doAggregate(MongoCollection<TResult> collection, BsonDocument bson) {
            return collection.aggregate(clientSession, List.of(bson), BsonDocument.class);
        }
    }

    /**
     * Ici, on réalise chaque opération sans transaction.
     */
    private static class WithoutTransactionSupport extends SomeDatabaseOperationWithMongoDb {
        public WithoutTransactionSupport(AutoCloseable mongoToClose, MongoClient mongoClient) {
            super(mongoToClose, mongoClient);
        }

        @Override
        public void commit() {
            // surtout, ne rien faire, la transaction n'est pas gérée
        }

        @Override
        protected long doCount(MongoCollection<Agent> collection) {
            return collection.countDocuments();
        }

        @Override
        protected void doInsert(MongoCollection<Agent> collection, List<Agent> students) {
            collection.insertMany(students);
        }

        @Override
        protected FindIterable<Agent> doFind(MongoCollection<Agent> collection, Bson bson) {
            return collection.find(bson);
        }

        @Override
        protected FindIterable<Agent> doFind(MongoCollection<Agent> collection) {
            return collection.find();
        }

        @Override
        protected String doCreateIndex(MongoCollection<Agent> collection, Bson index) {
            return collection.createIndex(index);
        }

        @Override
        protected <TResult> AggregateIterable<BsonDocument> doAggregate(MongoCollection<TResult> collection, BsonDocument bson) {
            return collection.aggregate(List.of(bson), BsonDocument.class);
        }
    }
}
