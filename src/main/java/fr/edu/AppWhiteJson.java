package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 * 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * http://ec.europa.eu/idabc/eupl5
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.util.List;

/**
 * Une classe visant à centraliser la (dé-)sérialisation JSON afin de s'assurer qu'elle est homogène.
 */
public class AppWhiteJson {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .registerModule(new JavaTimeModule());

    public String toJsonString(Object value) {
        final String json;
        try {
            json = OBJECT_MAPPER.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("incapable de convertir en JSON", e);
        }
        return json;
    }

    public <T> T parse(Class<T> valueType, String json) {
        try {
            return OBJECT_MAPPER.readValue(json, valueType);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("incapable de décoder " + json + " vers " + valueType, e);
        }
    }

    public <T> List<T> parseArray(Class<T> valueType, String json) {
        try {
            return OBJECT_MAPPER.readerForListOf(valueType).readValue(json);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("incapable de décoder " + json + " vers " + valueType, e);
        }
    }
}
