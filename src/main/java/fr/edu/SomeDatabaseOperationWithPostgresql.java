package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 * 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * http://ec.europa.eu/idabc/eupl5
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Implémentation de {@link SomeDatabaseOperation} avec JBBC et PostgreSQL
 */
public class SomeDatabaseOperationWithPostgresql implements SomeDatabaseOperation {

    private static final AutoCloseable NO_DATABASE_TO_CLOSE = () -> {};

    private static final Logger logger = LogManager.getLogger(SomeDatabaseOperationWithPostgresql.class);

    private static final AppWhiteJson APP_WHITE_JSON = new AppWhiteJson();

    private final AutoCloseable postgresqlToClose;

    private final Connection connection;

    private AgentTable agentTable;

    private CongeTable congeTable;

    private CarriereHistoriqueTable carriereHistoriqueTable;

    private SomeDatabaseOperationWithPostgresql(AutoCloseable postgresqlToClose, Connection connection) {
        this.postgresqlToClose = postgresqlToClose;
        this.connection = connection;
    }

    public static SomeDatabaseOperationWithPostgresql forCredentials(String host, String port, String database, String user, String password, boolean autoCommit) {
        return forCredentials(host, port, database, user, password, NO_DATABASE_TO_CLOSE, autoCommit);
    }

    public static SomeDatabaseOperationWithPostgresql forCredentials(String host, String port, String database, String user, String password, AutoCloseable databaseToClose, boolean autoCommit) {
        final String url = String.format("jdbc:postgresql://%s:%s/%s", host, port, database);
        return forUrl(url, user, password, databaseToClose, autoCommit);
    }

    public static SomeDatabaseOperationWithPostgresql forUrl(String url, String user, String password, AutoCloseable postgresqlToClose, boolean autoCommit) {
        try {
            Connection conn = DriverManager.getConnection(url, user, password);
            conn.setAutoCommit(autoCommit);
            final SomeDatabaseOperationWithPostgresql forUrl = new SomeDatabaseOperationWithPostgresql(postgresqlToClose, conn);
            forUrl.init();
            return forUrl;
        } catch (SQLException e) {
            throw new RuntimeException("impossible de se connecter à la base PostgreSQL locale", e);
        }
    }

    private void init() {
        // on crée la fonction pour jsonb_array_contains_any_text qui est un alias pour l'opérateur '?|' qu'on ne peut pas utiliser avec JDBC (à cause de ? qui est utilisé comme paramètre)
        try (Statement st = connection.createStatement()) {
            String sql = "CREATE FUNCTION jsonb_array_contains_any_text(x jsonb, y text[]) RETURNS boolean LANGUAGE SQL IMMUTABLE RETURN x ?| y";
            st.execute(sql);
        } catch (SQLException e) {
            throw new RuntimeException("impossible de créer la fonction 'jsonb_array_contains_any_text'", e);
        }
        // on crée la fonction json_text_array_to_array qui transforme un tableau JSON en tableau (au sens array SQL)
        try (Statement st = connection.createStatement()) {
            String sql = "CREATE FUNCTION json_text_array_to_array(jsonArray jsonb) RETURNS text[] LANGUAGE SQL IMMUTABLE RETURN ("
                       + "  SELECT array_agg(elements)"
                       + "  FROM jsonb_array_elements_text(jsonArray) as elements"
                       + ")";
            st.execute(sql);
        } catch (SQLException e) {
            throw new RuntimeException("impossible de créer la fonction 'jsonb_array_contains_any_text'", e);
        }
        agentTable = new AgentTable();
        agentTable.init();
        congeTable = new CongeTable();
        congeTable.init();
        carriereHistoriqueTable = new CarriereHistoriqueTable();
        carriereHistoriqueTable.init();
    }

    @Override
    public void commit() {
        try {
            if (connection.getAutoCommit()) {
                logger.trace("déjà en autoCommit, on ne commit pas");
            } else {
                connection.commit();
            }
        } catch (SQLException e) {
            throw new RuntimeException("incapable de commit", e);
        }
    }

    @Override
    public Agent findAgentById(String agentId) {
        return agentTable.findById(agentId);
    }

    @Override
    public List<Agent> findAgentsByIds(Set<String> agentIds) {
        return agentTable.findByIds(agentIds);
    }

    @Override
    public List<Agent> findAgentByNir(String nir) {
        return agentTable.findByNir(nir);
    }

    @Override
    public void insert(List<Agent> agents) {
        agentTable.insert(agents);
    }

    @Override
    public int count() {
        return agentTable.count();
    }

    @Override
    public void close() {
        try {
            logger.info("fermeture de la connexion à PostgreSQL");
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException("erreur lors de la fermeture de la connexion à PostgreSQL", e);
        }
        try {
            logger.info("fermeture de PostgreSQL");
            postgresqlToClose.close();
        } catch (Exception e) {
            throw new RuntimeException("impossible de fermer l'instance PostgreSQL " + postgresqlToClose, e);
        }
    }

    @Override
    public void forEachAgent(Consumer<? super Agent> consumer) {
        agentTable.forEach(consumer);
    }

    @Override
    public List<Agent> searchAgents(String dateFinIso) {
        return agentTable.search(dateFinIso);
    }

    @Override
    public List<Agent> searchAgents(String dateIso, Set<String> nirs, Set<String> carriereGradeCodes) {
        return agentTable.search(dateIso, nirs, carriereGradeCodes);
    }

    @Override
    public List<Agent> searchAgents(Set<String> carriereGradeCodes) {
        return agentTable.search(carriereGradeCodes);
    }

    @Override
    public void indexAgentsDateFin() {
        agentTable.indexDateFin();
    }

    @Override
    public void indexAgentNirsUsingHash() {
        agentTable.indexNirsUsingHash();
    }

    @Override
    public void indexAgentsDateFinDateDebutNir() {
        agentTable.indexDateFinDateDebutNir();
    }

    @Override
    public void indexAgentsCarriereGradeCodes() {
        agentTable.indexCarriereGradeCodes();
    }

    @Override
    public int sumNombreConges() {
        return congeTable.sumNombreConges();
    }

    @Override
    public Map<String, Integer> countAgentPerDepartementNaissance() {
        return agentTable.countPerDepartementNaissance();
    }

    private abstract class Table<E> {

        protected abstract Class<E> getDocumentType();

        protected abstract String getTableName();

        public void init() {
            createTable();
            if (false) {
                createGinIndex();
            }
            analyse();
            warmUp();
        }

        /**
         * Préchargement des données en mémoire.
         */
        private void warmUp() {
            String sql = String.format("SELECT * FROM %s", getTableName());
            try (Statement st = connection.createStatement()) {
                st.execute(sql);
            } catch (SQLException e) {
                throw new RuntimeException("impossible de préparer la requête", e);
            }
        }

        public int count() {
            String sql = String.format("SELECT COUNT(id) FROM %s", getTableName());
            try (Statement st = connection.createStatement()) {
                try (ResultSet rs = st.executeQuery(sql)) {
                    rs.next();
                    return rs.getInt(1);
                }
            } catch (SQLException e) {
                throw new RuntimeException("impossible de préparer la requête", e);
            }
        }

        public void insert(List<E> documents) {
            final String json = APP_WHITE_JSON.toJsonString(documents);
            final String sql = String.format("INSERT INTO %s(data) (SELECT value AS data FROM JSONB_ARRAY_ELEMENTS(?::JSONB))", getTableName());
            try (PreparedStatement insertStatement = connection.prepareStatement(sql)) {
                insertStatement.setString(1, json);
                insertStatement.execute();
            } catch (SQLException e) {
                throw new RuntimeException("impossible de préparer la requête", e);
            }
        }

        public void createTable() {
            try (Statement st = connection.createStatement()) {
                String sql = String.format("CREATE SEQUENCE IF NOT EXISTS %s", getSequenceName());
                st.execute(sql);
            } catch (SQLException e) {
                throw new RuntimeException("impossible de créer la sequence " + getSequenceName(), e);
            }
            try (Statement st = connection.createStatement()) {
                String sql = String.format("CREATE TABLE IF NOT EXISTS %s (id CHAR(24) PRIMARY KEY DEFAULT nextval('%s'), data jsonb)", getTableName(), getSequenceName());
                st.execute(sql);
            } catch (SQLException e) {
                throw new RuntimeException("impossible de créer la table " + getTableName(), e);
            }
        }

        public void createGinIndex() {
            try (Statement st = connection.createStatement()) {
                String sql = String.format("CREATE INDEX IF NOT EXISTS %s_data_idx ON %s USING GIN (data)", getTableName(), getTableName());
                st.execute(sql);
            } catch (SQLException e) {
                throw new RuntimeException("impossible de créer l'index GIN sur " + getTableName(), e);
            }
            analyse();
        }

        protected void analyse() {
            try (Statement st = connection.createStatement()) {
                String sql = String.format("ANALYSE %s", getTableName());
                st.execute(sql);
            } catch (SQLException e) {
                throw new RuntimeException("impossible d'analyser la table " + getTableName(), e);
            }
        }

        private String getSequenceName() {
            return getTableName() + "_sequence";
        }

        public E findById(String id) {
            String sql = String.format("SELECT data FROM %s WHERE id = ?", getTableName());
            try (PreparedStatement selectStatement = connection.prepareStatement(sql)) {
                selectStatement.setString(1, id);
                try (ResultSet resultSet = selectStatement.executeQuery()) {
                    resultSet.next();
                    String json = resultSet.getString(1);
                    E document = APP_WHITE_JSON.parse(getDocumentType(), json);
                    return document;
                }
            } catch (SQLException e) {
                throw new RuntimeException("impossible de préparer la requête", e);
            }
        }

        public List<E> findByIds(Set<String> ids) {
            // plus lent, l'index sur l'id n'est pas utilisé
//            String sql = String.format("SELECT data FROM %s WHERE ?::JSONB ? id", tableName);
            // plus lent, ça remonte x records alors qu'on peut n'en remonter qu'un avec un tableau JSON
//            String sql = String.format("SELECT data FROM %s WHERE id IN (SELECT value::CHAR(24) FROM JSONB_ARRAY_ELEMENTS_TEXT(?::JSONB))", tableName);
            String sql = String.format("SELECT json_agg(data) FROM %s WHERE id IN (SELECT value::CHAR(24) FROM JSONB_ARRAY_ELEMENTS_TEXT(?::JSONB))", getTableName());
            try (PreparedStatement selectStatement = connection.prepareStatement(sql)) {
                selectStatement.setString(1, APP_WHITE_JSON.toJsonString(ids));
                try (ResultSet resultSet = selectStatement.executeQuery()) {
                    resultSet.next();
                    String json = resultSet.getString(1);
                    List<E> documents = APP_WHITE_JSON.parseArray(getDocumentType(), json);
                    return documents;
                }
            } catch (SQLException e) {
                throw new RuntimeException("impossible de préparer la requête", e);
            }
        }

        public void forEach(Consumer<? super E> consumer) {
            //conn.setAutoCommit(false);
            //try (Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)) {
            try (Statement statement = connection.createStatement()) {
                statement.setFetchSize(50);
                String sql = String.format("SELECT data FROM %s", getTableName());
                try (ResultSet resultSet = statement.executeQuery(sql)) {
                    while (resultSet.next()) {
                        String json = resultSet.getString(1);
                        E document = APP_WHITE_JSON.parse(getDocumentType(), json);
                        consumer.accept(document);
                    }
                }
            } catch (SQLException e) {
                throw new RuntimeException("impossible de préparer la requête", e);
            }
        }
    }

    private class AgentTable extends Table<Agent> {

        @Override
        protected Class<Agent> getDocumentType() {
            return TableReference.agents().getDocumentType();
        }

        @Override
        protected String getTableName() {
            return TableReference.agents().getTableName();
        }

        public List<Agent> search(String dateFinIso) {
            final long dateFinTimeStamp = dateIsoToTimestamp(dateFinIso);
            String sql = String.format("SELECT json_agg(data) FROM %s WHERE (data->>'dateFin')::bigint >= ?", getTableName());
            try (PreparedStatement selectStatement = connection.prepareStatement(sql)) {
                selectStatement.setLong(1, dateFinTimeStamp);
                try (ResultSet resultSet = selectStatement.executeQuery()) {
                    resultSet.next();
                    String json = resultSet.getString(1);
                    List<Agent> documents = APP_WHITE_JSON.parseArray(getDocumentType(), json);
                    return documents;
                }
            } catch (SQLException e) {
                throw new RuntimeException("impossible de préparer la requête", e);
            }
        }

        public List<Agent> search(String dateIso, Set<String> nirs, Set<String> carriereGradeCodes) {
            final long dateTimeStamp = dateIsoToTimestamp(dateIso);
            String sql;
            {
                String whereClause = String.join(" AND ",
                        "(data->>'dateFin')::bigint >= ?",
                        "(data->>'dateDebut')::bigint <= ?",
                        "(data->>'nir') IN (SELECT value FROM JSONB_ARRAY_ELEMENTS_TEXT(?::JSONB))",
                        "(select array_agg(codes)::text[] FROM jsonb_array_elements_text(jsonb_path_query_array(data, '$.carrieres[*].grade.code')) as codes) && (select array_agg(codes)::text[] FROM jsonb_array_elements_text(?::JSONB) as codes)"
                );
                sql = String.format("SELECT json_agg(data) FROM %s WHERE " + whereClause, getTableName());
            }
            {
                String whereClause = String.join(" AND ",
                        "(data->>'dateFin')::bigint >= ?",
                        "(data->>'dateDebut')::bigint <= ?",
                        "(data->>'nir') IN (SELECT value FROM JSONB_ARRAY_ELEMENTS_TEXT(?::JSONB))",
                        "id in (SELECT id FROM codes where code in (SELECT JSONB_ARRAY_ELEMENTS_TEXT(?::JSONB)))"
                );
                final String withClause = "WITH codes AS (\n" +
                        "            select id, code\n" +
                        "            from agent\n" +
                        "            JOIN LATERAL\n" +
                        "            UNNEST( \n" +
                        "                ARRAY(\n" +
                        "                    SELECT jsonb_array_elements_text(jsonb_path_query_array(data, '$.carrieres[*].grade.code'))\n" +
                        "                )\n" +
                        "            ) code ON TRUE\n" +
                        "            )";
                sql = withClause + " " + String.format("SELECT json_agg(data) FROM %s WHERE " + whereClause, getTableName());
            }
            {
                String whereClause = String.join(" AND ",
                        "(data->>'dateFin')::bigint >= ?",
                        "(data->>'dateDebut')::bigint <= ?",
                        "(data->>'nir') IN (SELECT value FROM JSONB_ARRAY_ELEMENTS_TEXT(?::JSONB))",
                        "code in (SELECT JSONB_ARRAY_ELEMENTS_TEXT(?::JSONB))"
                );
                final String withClause = "WITH selected AS ("
                        + "  SELECT id, code"
                        + "  FROM agent"
                        + "    JOIN LATERAL UNNEST(ARRAY(SELECT jsonb_array_elements_text(jsonb_path_query_array(data, '$.carrieres[*].grade.code')))) code ON TRUE"
                        + "  WHERE " + whereClause
                        + ")";
                final String select = "json_agg(data)";
                sql = String.join("\n"
                        , withClause
                        , "SELECT " + select
                        , "FROM " + getTableName()
                        , "WHERE id IN (SELECT id FROM selected)"
                );
            }
            try (PreparedStatement selectStatement = connection.prepareStatement(sql)) {
                selectStatement.setLong(1, dateTimeStamp);
                selectStatement.setLong(2, dateTimeStamp);
                selectStatement.setString(3, APP_WHITE_JSON.toJsonString(nirs));
                selectStatement.setString(4, APP_WHITE_JSON.toJsonString(carriereGradeCodes));
                try (ResultSet resultSet = selectStatement.executeQuery()) {
                    resultSet.next();
                    String json = resultSet.getString(1);
                    List<Agent> documents = APP_WHITE_JSON.parseArray(getDocumentType(), json);
                    return documents;
                }
            } catch (SQLException e) {
                throw new RuntimeException("impossible de préparer la requête", e);
            }
        }

        public void indexDateFin() {
            try (Statement st = connection.createStatement()) {
                String sql = String.format("CREATE INDEX ON %s (((data->>'dateFin')::BIGINT))", getTableName());
                st.execute(sql);
            } catch (SQLException e) {
                throw new RuntimeException("impossible de créer l'index sur dateFin' " + getTableName(), e);
            }
//            try (Statement st = connection.createStatement()) {
//                String sql = String.format("CREATE STATISTICS agentstat ON (((data->>'dateFin')::BIGINT)) FROM %s", getTableName());
//                st.execute(sql);
//            } catch (SQLException e) {
//                throw new RuntimeException("impossible d'ajouter les stats sur " + getTableName(), e);
//            }
            analyse();
        }

        public List<Agent> findByNir(String nir) {
            String sql = String.format("SELECT json_agg(data) FROM %s WHERE (data->>'nir') = ?", getTableName());
            try (PreparedStatement selectStatement = connection.prepareStatement(sql)) {
                selectStatement.setString(1, nir);
                try (ResultSet resultSet = selectStatement.executeQuery()) {
                    resultSet.next();
                    String json = resultSet.getString(1);
                    List<Agent> documents = APP_WHITE_JSON.parseArray(getDocumentType(), json);
                    return documents;
                }
            } catch (SQLException e) {
                throw new RuntimeException("impossible de préparer la requête", e);
            }
        }

        public List<Agent> search(Set<String> carriereGradeCodes) {
            String sql = String.format("SELECT json_agg(data) FROM %s WHERE json_text_array_to_array(jsonb_path_query_array(data, '$.carrieres[*].grade.code')) && json_text_array_to_array(?::JSONB)", getTableName());
            try (PreparedStatement selectStatement = connection.prepareStatement(sql)) {
                selectStatement.setString(1, APP_WHITE_JSON.toJsonString(carriereGradeCodes));
                try (ResultSet resultSet = selectStatement.executeQuery()) {
                    resultSet.next();
                    String json = resultSet.getString(1);
                    List<Agent> documents = APP_WHITE_JSON.parseArray(getDocumentType(), json);
                    return documents;
                }
            } catch (SQLException e) {
                throw new RuntimeException("impossible de préparer la requête", e);
            }
        }

        public void indexNirsUsingHash() {
            try (Statement st = connection.createStatement()) {
                String sql = String.format("CREATE INDEX ON %s USING HASH ((data->>'nir'))", getTableName());
                st.execute(sql);
            } catch (SQLException e) {
                throw new RuntimeException("impossible de créer l'index sur nir' " + getTableName(), e);
            }
            analyse();
        }

        public void indexDateFinDateDebutNir() {
            try (Statement st = connection.createStatement()) {
                String sql = String.format("CREATE INDEX ON %s (((data->>'dateFin')::BIGINT), ((data->>'dateDebut')::BIGINT), ((data->>'nir')::text))", getTableName());
                st.execute(sql);
            } catch (SQLException e) {
                throw new RuntimeException("impossible de créer l'index sur dateFin' " + getTableName(), e);
            }
            analyse();
        }

        public void indexCarriereGradeCodes() {
            try (Statement st = connection.createStatement()) {
                //String sql = String.format("CREATE INDEX ON %s USING GIN (JSONB_PATH_QUERY_ARRAY(data, '$.carrieres[*].grade.code'))", getTableName());
                String sql = String.format("CREATE INDEX ON %s USING GIN (JSON_TEXT_ARRAY_TO_ARRAY(JSONB_PATH_QUERY_ARRAY(data, '$.carrieres[*].grade.code')))", getTableName());
                st.execute(sql);
            } catch (SQLException e) {
                throw new RuntimeException("impossible de créer l'index sur 'carrieres'" + getTableName(), e);
            }
            analyse();
        }

        public Map<String, Integer> countPerDepartementNaissance() {
            String sql = String.format("SELECT data['etatCivil']['deptNaissance'], COUNT(id) FROM %s GROUP BY data['etatCivil']['deptNaissance']", getTableName());
            try (PreparedStatement selectStatement = connection.prepareStatement(sql)) {
                try (ResultSet resultSet = selectStatement.executeQuery()) {
                    Map<String, Integer> countPerDepartementNaissance = new HashMap<>();
                    while (resultSet.next()) {
                        countPerDepartementNaissance.put(resultSet.getString(1), resultSet.getInt(2));
                    }
                    return countPerDepartementNaissance;
                }
            } catch (SQLException e) {
                throw new RuntimeException("impossible de préparer la requête", e);
            }
        }
    }

    private long dateIsoToTimestamp(String dateFinIso) {
        return DateTimeFormatter.ISO_DATE_TIME.parse(dateFinIso).getLong(ChronoField.INSTANT_SECONDS) * 1000;
    }

    private class CongeTable extends Table<Conge> {

        @Override
        protected Class<Conge> getDocumentType() {
            return TableReference.conges().getDocumentType();
        }

        @Override
        protected String getTableName() {
            return TableReference.conges().getTableName();
        }

        public int sumNombreConges() {
            String sql = String.format("SELECT SUM((data->>'nombreConges')::int4) FROM %s", getTableName());
            try (Statement st = connection.createStatement()) {
                try (ResultSet rs = st.executeQuery(sql)) {
                    rs.next();
                    return rs.getInt(1);
                }
            } catch (SQLException e) {
                throw new RuntimeException("impossible de préparer la requête", e);
            }
        }
    }

    private class CarriereHistoriqueTable extends Table<CarriereHistorique> {

        @Override
        protected Class<CarriereHistorique> getDocumentType() {
            return TableReference.carriereHistoriques().getDocumentType();
        }

        @Override
        protected String getTableName() {
            return TableReference.carriereHistoriques().getTableName();
        }
    }
}
