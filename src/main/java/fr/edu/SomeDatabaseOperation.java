package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 * 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * http://ec.europa.eu/idabc/eupl5
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Représente les opérations communes que MongoDB et PostgreSQL devront faire pendant les essais.
 */
public interface SomeDatabaseOperation extends AutoCloseable {

    Agent findAgentById(String agentId);

    List<Agent> findAgentByNir(String nir);

    List<Agent> findAgentsByIds(Set<String> agentIds);

    void insert(List<Agent> agents);

    int count();

    void commit();

    @Override
    void close();

    void forEachAgent(Consumer<? super Agent> consumer);

    List<Agent> searchAgents(String dateFinIso);

    List<Agent> searchAgents(String dateIso, Set<String> nirs, Set<String> carriereGradeCodes);

    List<Agent> searchAgents(Set<String> carriereGradeCodes);

    void indexAgentsDateFin();

    void indexAgentNirsUsingHash();

    void indexAgentsDateFinDateDebutNir();

    void indexAgentsCarriereGradeCodes();

    int sumNombreConges();

    Map<String, Integer> countAgentPerDepartementNaissance();
}
