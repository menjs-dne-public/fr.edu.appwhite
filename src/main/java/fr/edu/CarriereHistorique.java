package fr.edu;

/*-
 * #%L
 * AppWhite
 * %%
 * Copyright (C) 2021 Ministère de l’Éducation Nationale
 * %%
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission - subsequent versions of the
 * EUPL (the "Licence");
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 * #L%
 */

import java.util.Collection;
import java.util.Date;

/**
 * Un Java bean pour représenter un document de la collection "carrierehistorique"
 */
public class CarriereHistorique {
    private String _id;
    private String nir;
    private String numen;
    private String _class;
    private Collection<Statut> statuts;
    private String typeNsi;
    private String idSource;
    private Collection<Carrieres> carrieres;
    private String matricule;
    private Collection<Categories> categories;
    private String lotGestion;
    private Collection<PeriodesActivites> periodesActivites;
    private RecrutementInitial recrutementInitial;
    private Collection<PeriodesServiceNational> periodesServiceNational;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getNir() {
        return nir;
    }

    public void setNir(String nir) {
        this.nir = nir;
    }

    public String getNumen() {
        return numen;
    }

    public void setNumen(String numen) {
        this.numen = numen;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public Collection<Statut> getStatuts() {
        return statuts;
    }

    public void setStatuts(Collection<Statut> statuts) {
        this.statuts = statuts;
    }

    public String getTypeNsi() {
        return typeNsi;
    }

    public void setTypeNsi(String typeNsi) {
        this.typeNsi = typeNsi;
    }

    public String getIdSource() {
        return idSource;
    }

    public void setIdSource(String idSource) {
        this.idSource = idSource;
    }

    public Collection<Carrieres> getCarrieres() {
        return carrieres;
    }

    public void setCarrieres(Collection<Carrieres> carrieres) {
        this.carrieres = carrieres;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public Collection<Categories> getCategories() {
        return categories;
    }

    public void setCategories(Collection<Categories> categories) {
        this.categories = categories;
    }

    public String getLotGestion() {
        return lotGestion;
    }

    public void setLotGestion(String lotGestion) {
        this.lotGestion = lotGestion;
    }

    public Collection<PeriodesActivites> getPeriodesActivites() {
        return periodesActivites;
    }

    public void setPeriodesActivites(Collection<PeriodesActivites> periodesActivites) {
        this.periodesActivites = periodesActivites;
    }

    public RecrutementInitial getRecrutementInitial() {
        return recrutementInitial;
    }

    public void setRecrutementInitial(RecrutementInitial recrutementInitial) {
        this.recrutementInitial = recrutementInitial;
    }

    public Collection<PeriodesServiceNational> getPeriodesServiceNational() {
        return periodesServiceNational;
    }

    public void setPeriodesServiceNational(Collection<PeriodesServiceNational> periodesServiceNational) {
        this.periodesServiceNational = periodesServiceNational;
    }

    public static class Carrieres {
        private Collection<Corps> corps;
        private Collection<TypesCarriere> typesCarriere;

        public Collection<Corps> getCorps() {
            return corps;
        }

        public void setCorps(Collection<Corps> corps) {
            this.corps = corps;
        }

        public Collection<TypesCarriere> getTypesCarriere() {
            return typesCarriere;
        }

        public void setTypesCarriere(Collection<TypesCarriere> typesCarriere) {
            this.typesCarriere = typesCarriere;
        }

        public static class Corps {

            private String code;
            private String typePers;
            private String modeAcces;
            private Date dateEntree;
            private Date dateFin;
            private String modeSortie;
            private Date dateIntegration;
            private Collection<Grades> grades;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getTypePers() {
                return typePers;
            }

            public void setTypePers(String typePers) {
                this.typePers = typePers;
            }

            public String getModeAcces() {
                return modeAcces;
            }

            public void setModeAcces(String modeAcces) {
                this.modeAcces = modeAcces;
            }

            public Date getDateEntree() {
                return dateEntree;
            }

            public void setDateEntree(Date dateEntree) {
                this.dateEntree = dateEntree;
            }

            public Date getDateFin() {
                return dateFin;
            }

            public void setDateFin(Date dateFin) {
                this.dateFin = dateFin;
            }

            public String getModeSortie() {
                return modeSortie;
            }

            public void setModeSortie(String modeSortie) {
                this.modeSortie = modeSortie;
            }

            public Date getDateIntegration() {
                return dateIntegration;
            }

            public void setDateIntegration(Date dateIntegration) {
                this.dateIntegration = dateIntegration;
            }

            public Collection<Grades> getGrades() {
                return grades;
            }

            public void setGrades(Collection<Grades> grades) {
                this.grades = grades;
            }

            public static class Grades {

                private String code;
                private String modeAcces;
                private Date dateEntree;
                private Date dateFin;
                private Collection<Echelons> echelons;

                public String getCode() {
                    return code;
                }

                public void setCode(String code) {
                    this.code = code;
                }

                public String getModeAcces() {
                    return modeAcces;
                }

                public void setModeAcces(String modeAcces) {
                    this.modeAcces = modeAcces;
                }

                public Date getDateEntree() {
                    return dateEntree;
                }

                public void setDateEntree(Date dateEntree) {
                    this.dateEntree = dateEntree;
                }

                public Date getDateFin() {
                    return dateFin;
                }

                public void setDateFin(Date dateFin) {
                    this.dateFin = dateFin;
                }

                public Collection<Echelons> getEchelons() {
                    return echelons;
                }

                public void setEchelons(Collection<Echelons> echelons) {
                    this.echelons = echelons;
                }

                public static class Echelons {

                    private String code;
                    private String grille;
                    private String modeAcces;
                    private Date dateEntree;
                    private Date dateFin;
                    private Collection<Chevrons> chevrons;

                    public String getCode() {
                        return code;
                    }

                    public void setCode(String code) {
                        this.code = code;
                    }

                    public String getGrille() {
                        return grille;
                    }

                    public void setGrille(String grille) {
                        this.grille = grille;
                    }

                    public String getModeAcces() {
                        return modeAcces;
                    }

                    public void setModeAcces(String modeAcces) {
                        this.modeAcces = modeAcces;
                    }

                    public Date getDateEntree() {
                        return dateEntree;
                    }

                    public void setDateEntree(Date dateEntree) {
                        this.dateEntree = dateEntree;
                    }

                    public Date getDateFin() {
                        return dateFin;
                    }

                    public void setDateFin(Date dateFin) {
                        this.dateFin = dateFin;
                    }

                    public Collection<Chevrons> getChevrons() {
                        return chevrons;
                    }

                    public void setChevrons(Collection<Chevrons> chevrons) {
                        this.chevrons = chevrons;
                    }

                    public static class Chevrons {

                        private String code;
                        private Date dateFin;
                        private Date dateEffet;

                        public String getCode() {
                            return code;
                        }

                        public void setCode(String code) {
                            this.code = code;
                        }

                        public Date getDateFin() {
                            return dateFin;
                        }

                        public void setDateFin(Date dateFin) {
                            this.dateFin = dateFin;
                        }

                        public Date getDateEffet() {
                            return dateEffet;
                        }

                        public void setDateEffet(Date dateEffet) {
                            this.dateEffet = dateEffet;
                        }
                    }
                }
            }
        }

        public static class TypesCarriere {
            private Date dateFin;
            private Date dateDebut;
            private String indicateur;
            private String typologiePop;

            public Date getDateFin() {
                return dateFin;
            }

            public void setDateFin(Date dateFin) {
                this.dateFin = dateFin;
            }

            public Date getDateDebut() {
                return dateDebut;
            }

            public void setDateDebut(Date dateDebut) {
                this.dateDebut = dateDebut;
            }

            public String getIndicateur() {
                return indicateur;
            }

            public void setIndicateur(String indicateur) {
                this.indicateur = indicateur;
            }

            public String getTypologiePop() {
                return typologiePop;
            }

            public void setTypologiePop(String typologiePop) {
                this.typologiePop = typologiePop;
            }
        }
    }

    public static class Categories {

        private String code;
        private Date dateFin;
        private Date dateEntree;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Date getDateFin() {
            return dateFin;
        }

        public void setDateFin(Date dateFin) {
            this.dateFin = dateFin;
        }

        public Date getDateEntree() {
            return dateEntree;
        }

        public void setDateEntree(Date dateEntree) {
            this.dateEntree = dateEntree;
        }
    }

    public static class PeriodesActivites {

        private String statut;
        private Date dateFin;
        private Date dateDebut;
        private String typeService;
        private String quotiteService;
        private String modaliteService;
        private int dureeValideeRetraite;

        public String getStatut() {
            return statut;
        }

        public void setStatut(String statut) {
            this.statut = statut;
        }

        public Date getDateFin() {
            return dateFin;
        }

        public void setDateFin(Date dateFin) {
            this.dateFin = dateFin;
        }

        public Date getDateDebut() {
            return dateDebut;
        }

        public void setDateDebut(Date dateDebut) {
            this.dateDebut = dateDebut;
        }

        public String getTypeService() {
            return typeService;
        }

        public void setTypeService(String typeService) {
            this.typeService = typeService;
        }

        public String getQuotiteService() {
            return quotiteService;
        }

        public void setQuotiteService(String quotiteService) {
            this.quotiteService = quotiteService;
        }

        public String getModaliteService() {
            return modaliteService;
        }

        public void setModaliteService(String modaliteService) {
            this.modaliteService = modaliteService;
        }

        public int getDureeValideeRetraite() {
            return dureeValideeRetraite;
        }

        public void setDureeValideeRetraite(int dureeValideeRetraite) {
            this.dureeValideeRetraite = dureeValideeRetraite;
        }
    }

    public static class RecrutementInitial {

        private Date dateEntreeFP;
        private String modeEntreeFP;
        private Date dateEntreeFPE;

        public Date getDateEntreeFP() {
            return dateEntreeFP;
        }

        public void setDateEntreeFP(Date dateEntreeFP) {
            this.dateEntreeFP = dateEntreeFP;
        }

        public String getModeEntreeFP() {
            return modeEntreeFP;
        }

        public void setModeEntreeFP(String modeEntreeFP) {
            this.modeEntreeFP = modeEntreeFP;
        }

        public Date getDateEntreeFPE() {
            return dateEntreeFPE;
        }

        public void setDateEntreeFPE(Date dateEntreeFPE) {
            this.dateEntreeFPE = dateEntreeFPE;
        }
    }

    public static class PeriodesServiceNational {

        private int duree;
        private Date dateFin;
        private Date dateDebut;
        private String formeAccompl;

        public int getDuree() {
            return duree;
        }

        public void setDuree(int duree) {
            this.duree = duree;
        }

        public Date getDateFin() {
            return dateFin;
        }

        public void setDateFin(Date dateFin) {
            this.dateFin = dateFin;
        }

        public Date getDateDebut() {
            return dateDebut;
        }

        public void setDateDebut(Date dateDebut) {
            this.dateDebut = dateDebut;
        }

        public String getFormeAccompl() {
            return formeAccompl;
        }

        public void setFormeAccompl(String formeAccompl) {
            this.formeAccompl = formeAccompl;
        }
    }

    public static class Statut {
        private String code;
        private Date dateFin;
        private Date dateEntree;
        private Date dateFinReelleStage;
        private Date datePrevueFinStage;
        private Date dateTitularisation;
        private String typeSituationStage;
        private String commentaireProlStage;
        private String dureeProlongationStage;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Date getDateFin() {
            return dateFin;
        }

        public void setDateFin(Date dateFin) {
            this.dateFin = dateFin;
        }

        public Date getDateEntree() {
            return dateEntree;
        }

        public void setDateEntree(Date dateEntree) {
            this.dateEntree = dateEntree;
        }

        public Date getDateFinReelleStage() {
            return dateFinReelleStage;
        }

        public void setDateFinReelleStage(Date dateFinReelleStage) {
            this.dateFinReelleStage = dateFinReelleStage;
        }

        public Date getDatePrevueFinStage() {
            return datePrevueFinStage;
        }

        public void setDatePrevueFinStage(Date datePrevueFinStage) {
            this.datePrevueFinStage = datePrevueFinStage;
        }

        public Date getDateTitularisation() {
            return dateTitularisation;
        }

        public void setDateTitularisation(Date dateTitularisation) {
            this.dateTitularisation = dateTitularisation;
        }

        public String getTypeSituationStage() {
            return typeSituationStage;
        }

        public void setTypeSituationStage(String typeSituationStage) {
            this.typeSituationStage = typeSituationStage;
        }

        public String getCommentaireProlStage() {
            return commentaireProlStage;
        }

        public void setCommentaireProlStage(String commentaireProlStage) {
            this.commentaireProlStage = commentaireProlStage;
        }

        public String getDureeProlongationStage() {
            return dureeProlongationStage;
        }

        public void setDureeProlongationStage(String dureeProlongationStage) {
            this.dureeProlongationStage = dureeProlongationStage;
        }
    }
}











